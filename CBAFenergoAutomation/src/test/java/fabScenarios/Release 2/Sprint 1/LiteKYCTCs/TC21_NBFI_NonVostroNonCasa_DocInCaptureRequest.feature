#Test Case: TC21_NBFI_NonVostroNonCasa : Doc Validation
#Designed by: Vibhav Kumar
#Last Edited by: Vibhav Kumar
Feature: TC21_NBFI_NonVostroNonCasa : Doc Validation

  @Automation
  Scenario: Verify RM is able to upload documents on Capture request details screen when Client Type is "NBFI" and Category "Financial Institutions-Banks (Non-Vostro Relationship)/NBFI Non-CASA Relationship"
    Given I login to Fenergo Application with "RM:NBFI"
    When I navigate to "LegalEntityCategoryWithoutSubmit" button with ClientType as "Non-Bank Financial Institution (NBFI)"
    When I select "Financial Institutions-Banks (Non-Vostro Relationship)/NBFI Non-CASA Relationship" for "Dropdown" field "Legal Entity Category"
    When I select "Client/Counterparty" for "Dropdown" field "Legal Entity Role"
    When I select "Bank - France Branch" for "Dropdown" field "Entity of Onboarding"
    When I select "Financial Institutions-Banks (Non-Vostro Relationship)/NBFI Non-CASA Relationship" for "Dropdown" field "Legal Entity Category"
    When I select "Yes" for "Dropdown" field "Does the CDD profile qualify for Lite KYC?"
    And I click on "CreateEntity" button           
    When I complete "CaptureNewRequest" with Key "NBFI" and below data
      | Product | Relationship |
      | C1      | C1           |
    When I complete "Documents" in "Capture Request Details" screen
    And I click on "Continue" button
    
    
    
    
    
