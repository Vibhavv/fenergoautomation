#Test Case: TC22_FI_PensionFunds-DocInStage1
#Designed by: Vibhav Kumar
#Last Edited by: Vibhav Kumar
Feature: TC22_FI_PensionFunds-DocInStage1

  @Automation
  Scenario: Verify RM is able to upload documents on Capture request details screen when Client Type is "FI" and Category "Pension Fund"
    
    Given I login to Fenergo Application with "RM:FI"
    When I navigate to "LegalEntityCategoryWithoutSubmit" button with ClientType as "Financial Institution (FI)"
    When I select "Pension Funds" for "Dropdown" field "Legal Entity Category"    
    When I select "Client/Counterparty" for "Dropdown" field "Legal Entity Role"
    When I select "Bank - France Branch" for "Dropdown" field "Entity of Onboarding"
    When I select "Pension Funds" for "Dropdown" field "Legal Entity Category"
    When I select "Yes" for "Dropdown" field "Does the CDD profile qualify for Lite KYC?"
    And I click on "CreateEntity" button    
    And I complete "CaptureNewRequest" with Key "LiteKYC-FI" and below data
      | Product | Relationship |
      | C1      | C1           |
    When I complete "Documents" in "Capture Request Details" screen
    And I click on "Continue" button  