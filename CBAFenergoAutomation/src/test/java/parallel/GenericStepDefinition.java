package parallel;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.sql.ResultSet;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;



public class GenericStepDefinition extends PageBase {
	JavaScriptControls js=new JavaScriptControls();
    WebDriver driver= new ChromeDriver();	
	public static JavascriptExecutor jsExecutor;
	ScenarioContext scenariocontext=new ScenarioContext();
	
	Connection conn=null;
	CommonSteps commonsteps=new CommonSteps();
	
	public void clear_field(String fieldName) {
		js.clear(fieldName);
	}
	
    public static String getXpathForMandatoryByDocName(String documentName  ){
        //with document name we can get value of Mandatory field. this returns xpath of Mandatory field.
        String docXpath=    "(//*[contains(text(),'"+documentName+"')]";
        String locator2="//ancestor::div[@class='fen-datagrid-expandablerow__cell']//following-sibling::div//child::span[contains(@class,'fen-dataGrid-cell')])[4]";
        return docXpath+locator2;
        
  }
    //returns xpath of RiskRating when input is labelName. 
    //For e.g. to xpath of risk rating of label "Length of Relationship:", just use below method 
    public static String getXpathOfRiskRatingByLabelName(String labelName){
       String str = "//label[text()='"+labelName+"']//ancestor::div[@class=' ']//descendant::span[@class='read-only'][2]";
       return str;
    }
  //Use below method when risk rating is generated for group of labels. For e.g. 'Main Entity / Association Screening Risk' or 
    // field  'Association Country Risk' has one risk rating based on 4-5 labels. 
    public static String getXpathOfRiskRatingByGroupName(String groupName){
       String str = "//span[text()='"+groupName+"']//following-sibling::div[@class=' ']//descendant::span[@class='read-only'][2]";
       return str;
    }


	
	public void SelectElement(String fieldType,String label,String value) {
		
		switch(fieldType) {
		case "MultiSelectDropdown":
			MultiSelect.SelectMultipleElementsByText(label,value);
			break;
			
		case "Dropdown":
		Dropdown.SelectElementByText(label,value);
		
		case "TextBox":
			TextBox.selectElementbyText(label,value);
		
		}
	}
    static String getUpperCaseString(int n) 
    { 
  
        // chose a Character random from this String 
       
        String upperCaseString="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  
        // create StringBuffer size of AlphaNumericString 
        StringBuilder sb = new StringBuilder(n); 
  
        for (int i = 0; i < n; i++) { 
  
            // generate a random number between 
            // 0 to AlphaNumericString variable length 
            int index 
                = (int)(upperCaseString.length() 
                        * Math.random()); 
  
            // add Character one by one in end of sb 
            sb.append(upperCaseString 
                          .charAt(index)); 
        } 
  
        return sb.toString(); 
    }

 static String getLowerCaseString(int n) 
    { 
  
        // chose a Character random from this String 
       
        String lowerCaseString="abcdefghijklmnopqrstuvxyz";
  
        // create StringBuffer size of AlphaNumericString 
        StringBuilder sb = new StringBuilder(n); 
  
        for (int i = 0; i < n; i++) { 
  
            // generate a random number between 
            // 0 to AlphaNumericString variable length 
            int index 
                = (int)(lowerCaseString.length() 
                        * Math.random()); 
  
            // add Character one by one in end of sb 
            sb.append(lowerCaseString 
                          .charAt(index)); 
        } 
  
        return sb.toString(); 
    }

 static String getSpecialCharacterString(int n) 
    { 
  
        // chose a Character random from this String 
       
        String specialCharStrin="!@#$%^&*()_+/*-;:',.<>?";
  
        // create StringBuffer size of AlphaNumericString 
        StringBuilder sb = new StringBuilder(n); 
  
        for (int i = 0; i < n; i++) { 
  
            // generate a random number between 
            // 0 to AlphaNumericString variable length 
            int index 
                = (int)(specialCharStrin.length() 
                        * Math.random()); 
  
            // add Character one by one in end of sb 
            sb.append(specialCharStrin 
                          .charAt(index)); 
        } 
  
        return sb.toString(); 
    }

	public String getControls(String Datakey) { 
		String xpath=null;
	try {
		SqliteConnection sql=SqliteConnection.getInstance();
		ResultSet rst=sql.getControlsData("Controls",Datakey);
		if(!rst.next()) {throw new Exception("Controls information not found");}
		else if(rst.getString("identificationType").equals("XPATH")) {xpath=rst.getString("Locator");}
	   rst.close();
	}
	catch(Exception e) {
		
	}
	return xpath;

  }
	
	public void user_fill_in_datakey(String tablename,String Datakey) {
		try {
			SqliteConnection sql=SqliteConnection.getInstance();
			ResultSet rst=sql.buildQuery(tablename, Datakey);
			FillInDataWithoutValidation(rst);
			
				
			
		}
		catch(Exception e){
			assertTrue(e.getLocalizedMessage(),false);
		}
	}
	
	
	public void user_fill_in_legacy_datakey(String tablename,String Datakey) {
		try {
			SqliteConnection sql=SqliteConnection.getInstance();
			ResultSet rst=sql.buildQuery(tablename, Datakey);
			FillInDataWithoutValidation(rst);
			
				
			
		}
		catch(Exception e){
			assertTrue(e.getLocalizedMessage(),false);
		}
	}
	
	 static String getAlphaNumericString(int n) 
	    { 
	  
	        // chose a Character random from this String 
	        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	                                    + "0123456789"
	                                    + "abcdefghijklmnopqrstuvxyz"; 
	        String NumericString="0123456789";
	  
	        // create StringBuffer size of AlphaNumericString 
	        StringBuilder sb = new StringBuilder(n); 
	  
	        for (int i = 0; i < n; i++) { 
	  
	            // generate a random number between 
	            // 0 to AlphaNumericString variable length 
	            int index 
	                = (int)(AlphaNumericString.length() 
	                        * Math.random()); 
	  
	            // add Character one by one in end of sb 
	            sb.append(AlphaNumericString 
	                          .charAt(index)); 
	        } 
	  
	        return sb.toString(); 
	    } 
	 
	 static String getNumericString(int n) 
	    { 
	  
	        // chose a Character random from this String 
	       
	        String NumericString="0123456789";
	  
	        // create StringBuffer size of AlphaNumericString 
	        StringBuilder sb = new StringBuilder(n); 
	  
	        for (int i = 0; i < n; i++) { 
	  
	            // generate a random number between 
	            // 0 to AlphaNumericString variable length 
	            int index 
	                = (int)(NumericString.length() 
	                        * Math.random()); 
	  
	            // add Character one by one in end of sb 
	            sb.append(NumericString 
	                          .charAt(index)); 
	        } 
	  
	        return sb.toString(); 
	    } 
	 static String getAlphabetString(int n) 
	    { 
	  
	        // chose a Character random from this String 
	       
	      
	        String AlphabetString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
	  
	        // create StringBuffer size of AlphaNumericString 
	        StringBuilder sb = new StringBuilder(n); 
	  
	        for (int i = 0; i < n; i++) { 
	  
	            // generate a random number between 
	            // 0 to AlphaNumericString variable length 
	            int index 
	                = (int)(AlphabetString.length() 
	                        * Math.random()); 
	  
	            // add Character one by one in end of sb 
	            sb.append(AlphabetString 
	                          .charAt(index)); 
	        } 
	  
	        return sb.toString(); 
	    } 
	 
	 
	
	public String GetFenergoId() {
		String jsArg1="return window.top.location.href.toString()";
		String jsArg2="return document.getElementByClassName('header-subtitle header subtitle')(0).href";
		String FenergoId="";
		for (int i=1;i<5;i++) {
			String title=js.performAndReturn(driver,"return document.title").toString();
			if(title.equals("LE Details")) {
				FenergoId=js.performJsAndReturn(driver,jsArg1).split("app/")[1].split("/")[0].toString();}
			else {
				FenergoId=js.performJsAndReturn(driver,jsArg2).split("app/")[1].split("/")[0].toString();
				
			}
				if(FenergoId!=null)
				{
					System.out.println("Fenergo Id is :" +FenergoId);
					break;}
				
				
				
			}
			if(FenergoId==null) {
				System.out.println("kajsksda");
			}
			return FenergoId;
			
		}
	
	
	public void FileUpload(String xpath, String filename)
	
	{
	FileUpload.fileuploadPage(commonsteps.getXpathFromControls(xpath),filename);
	    //js.waitForPageLoad();
	}
	
	
	public void clickgridviajs(String columnName) {
		try 
		{
			
			jsExecutor.executeScript("PageObjects.find({gridColumnName:'" +columnName+"'}).select()");
			Thread.sleep(200);
		}
		catch(Exception e) {
			assertTrue(e.getLocalizedMessage(),false);
		}
		
	}
	
	public void user_Select_Dropdown(String value,String dataKey) {
		try {
			
	SqliteConnection sql=new SqliteConnection();
	dataKey=commonsteps.InputParameter(dataKey);
	ResultSet rst=sql.getControlsData("Controls", dataKey);
	if(!rst.next()) {
		ResultSet rst2=sql.getControlsData("FieldData", dataKey);
		if(!rst2.next()) {
			throw new Exception("Controls not found for Datakey" +dataKey);
		}
		else {
			Dropdown.SelectElementByText(rst2.getString("Label"),value,rst.getString("Datakey"));}
		rst2.close();
	}
	else if(rst.getString("IdentificationType")!=null) {
		if(rst.getString("IdentificationType").equals("XPATH")) {
			Dropdown.selectDropdownvalue(rst.getString("locator"),value);
		}
		else {
			Dropdown.SelectElementByText(rst.getString("Label"),value);
		}
		rst.close();
		
	}
		}
		catch(Exception e) {
			assertTrue(e.getLocalizedMessage(),false);
		}
	}
	 public void enter_value_in_testbox_using_label(String value,String label,String dataKey) {
		 String expectedvalue =commonsteps.InputParameter(value);
		 TextBox.fillInTextBox(label,expectedvalue,dataKey);
		 
	 }
     public static String getXpathForActionsByDocName(String documentName  ){
         //with document name we can get xpath of Actions button. this method returns that. 
         String docXpath=    "//*[contains(text(),'"+documentName+"')]";
         String locator2="/ancestor::div[@class='fen-datagrid-expandablerow__column-container--unfixed']//preceding-sibling::div//child::span[@class='icon fen-icon-ellipsis undefined']";
         return docXpath+locator2;
   }
   
   public static String getXpathForDocExpandByDocName(String documentName  ){
         //with document name we can get xpath of Actions button. this method returns that. 
         String docXpath=    "//*[contains(text(),'"+documentName+"')]";
         String locator2="/ancestor::div[@class='fen-expandable-section__header']//preceding-sibling::div//div[@class='icon icon-angle-up right']";
         return docXpath+locator2;
   }
   
   public static String getXpathForUnlinkEllipsisByDocName(String documentName){
         
         String docXpath = "(//*[contains(text(),'"+documentName+"')]";
         String xpathchild1 = "//ancestor::div[contains(@class,'fen-expandable-section')]//following-sibling::div[contains(@class,'fen-datagrid-expandablerow')]";
         String xpathchild2 = "//div[@class='fen-flexbox-item'])[2]//descendant::span[@class='icon fen-icon-ellipsis undefined']";
         return docXpath+xpathchild1+xpathchild2;
   }
   
   public static String getXpathForLinkEllipsisByDocName(String documentName){
         
         String docXpath = "(//*[contains(text(),'"+documentName+"')]";
         String xpathchild1 = "//ancestor::div[contains(@class,'fen-expandable-section')]//following-sibling::div[contains(@class,'fen-datagrid-expandablerow')]";
         String xpathchild2 = "//div[@class='fen-flexbox-item'])[1]//descendant::span[@class='icon fen-icon-ellipsis undefined']";
         return docXpath+xpathchild1+xpathchild2;
   }
   
   public static String getXpathForLinkedStatusByDocName(String documentName  ){
         //with document name we can get value of Mandatory field. this returns xpath of Mandatory field.
         String docXpath=    "(//*[contains(text(),'"+documentName+"')]";
         String locator2="//ancestor::div[@class='fen-datagrid-expandablerow__cell']//following-sibling::div//child::span[contains(@class,'fen-dataGrid-cell')])[1]";
         return docXpath+locator2;
         
   }

	 
	 public void store_caseId(String key,String xpathkey) {
		 if(key.equalsIgnoreCase("FenergoId")) {
			 
			 scenariocontext.setValue(key,GetFenergoId());
		 }
		 
		 String xpath=commonsteps.getXpathFromControls(xpathkey);
		 try 
		 {String value=TextBox.getTextvaluelegacyPage(xpath);
		 value=value.contains("ID:")?value.replace("ID:",""):value;
		 scenariocontext.setValue(key,value);
		 System.out.println(key +":"+ value);
		 
		 }
		 catch(Exception e) {
			 assertTrue(e.getLocalizedMessage(),false);
		 }
	 }
	
	public void user_navigate_topage(String screenname,String xpath) {
		driver.findElement(By.xpath(commonsteps.getXpathFromControls(xpath))).click();;
	}
	
	
	public void user_navigate_to_le360() {
		
	}
	
	 public static String getXpathForAssignedTeamByTaskName(String taskName){
	        String xpath = "//td//div//a[@title='"+taskName+"']//ancestor::td//following-sibling::td//span[text()='CIB KYC Maker']";
	        return xpath;
	       }
	   
	    public static String getXpathForActionsButtonByTaskName(String taskName){
	        String xpath = "//td//div//a[@title='"+taskName+"']//ancestor::td//following-sibling::td//span[@class='icon fen-icon-ellipsis undefined']";
	        return xpath;
	       }
	      
	    public static String getXpathForStatusByTaskName(String taskName){
	        String xpath = "(//td//div//a[@title='"+taskName+"']//ancestor::td//following-sibling::td//div//span)[7]";
	        return xpath;
	       }
	    
	    public static String getXpathForPlusButtonBySubflowName(String subflowName){
	    	String staticPartString = "//parent::div//following-sibling::div[@class='fen-header-components']//child::i[@class='icon fen-icon-circle-outline']";
	    	return "//h3[text()='"+subflowName+"']"+staticPartString;
	    }
	
	
	}


