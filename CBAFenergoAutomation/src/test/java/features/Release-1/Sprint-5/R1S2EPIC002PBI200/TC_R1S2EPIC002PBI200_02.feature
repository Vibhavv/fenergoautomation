#Test Case: TC_R1S2EPIC002PBI200_02
#PBI: R1S2EPIC002PBI200
#User Story ID: US012
#Designed by: Priyanka Arora
Feature: TC_R1S2EPIC002PBI200_02 

@Automation 
Scenario: 
	Validate the hidden fields on "LE details" screen for the Associated LE added via Express addition for "individual" relationship on "Capture Hierarchy details" screen 

	Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I complete "NewRequest" screen with key "Corporate" 
	And I complete "CaptureNewRequest" with Key "C1" and below data 
		| Product | Relationship |
		| C1      | C1           |
    And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	#Given I login to Fenergo Application with "SuperUser"
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
    When I navigate to "EnrichKYCProfileGrid" task 
	And I take a screenshot 
	When I complete "AddAddress" task 
	When I complete "EnrichKYC" screen with key "C1" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId"	
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking
	#When I add "AssociatedParty" via express addition
	When I complete "AssociatedPartiesExpressAddition" screen with key "Individual"
	#When I select Legal Entity type as "Individual" and complete the task 	=> Program selects Individual LE from DB.
	When I complete "AssociationDetails" screen with key "DirectorIndividual"	
	#When I navigate to "CaptureHierarchydetails" task again
	When I "Navigate to Legal Entity" by right clicking 
	#Testdata: Verify below mentioned fields as hidden  on "LE details" screen
	#And I verify below mentioned fields as hidden  on "LE details" screen: 
	Then I check that below data is not visible
		|FieldLabel						|
		| Marital Status                  |
		| Registration Number             |
		| Directors Identification Number |
		| Email                           |
		| Phone No.                       |
		| Insider Status                  |
		| PEP / Sanctions                 |
		| Anticipated Activity of Account |
