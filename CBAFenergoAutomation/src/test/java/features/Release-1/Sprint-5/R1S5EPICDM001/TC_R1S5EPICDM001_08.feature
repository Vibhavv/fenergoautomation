#Test Case: TC_R1S5EPICDM001_08
#PBI: R1S5EPICDM001
#User Story ID: NA
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Ability to generate workflows on the migrated customer profile

 
  Scenario: Validate if other workflows can be triggered on top a migrate entity.
  #Precondition - Create a migrated entity using DM screen 1 and 2 and note the T24 CIF ID of the migrated entity
  
  Given I login to Fenergo Application with "Super User/DM User" 
  And I navigate to "Legal Entity Search" screen
  And I expand "Advanced Search"
  And I enter fill "T24 CIF ID" with T24 CIF ID value of the migrated entity
  And I assert that the migrated entity is visible in the result grid
  And I navigate to LE 360 screen of the migrated entity
  And I assert the following workflows are visible under Actions button
		#Agency Request
		#CRS Remediation
		#Maintenance Request
		#Margin Remediation
		#Reassessment
		#Regular Review
		#DCO Example
  
  