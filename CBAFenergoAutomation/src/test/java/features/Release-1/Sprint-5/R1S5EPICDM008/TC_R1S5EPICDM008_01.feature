#Test Case: TC_R1S5EPICDM008_01
#PBI: R1S5EPICDM008
#User Story ID: NA
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: DM Screen 2 - Association 

 
  Scenario: To validate the behaviour of "Search Result" grid in DM screen -2 
  #if results are fetched, they should be displayed in the Grid 
  #if results are not fetched, "No legal entities found" text should be displayed in the grid
  #Precondition - Migrated entity for which associated parties to be added should be created before 
  Given I login to Fenergo Application with "DM user"
  And I navigate to DM Screen-2 #using url
  And I validate the following fields under "Search Entity Details" section
   | Label                   | FieldType        | Visible | Editable | Mandatory |
   | T24 CIF ID              | Numeric          | true    | true     | true      |
  And I assert that button "Add Selected" is disabled
  #Provide random value for the "T24 CIF ID" field in the following step 
  And I provide value for "T24 CIF ID" field 
  And I click on "SEARCH" button
  And I assert that the following message is displayed in the grid
  |No legal entities found|
 	And I provide value for "T24 CIF ID" field of the migrated legal entity for which associations need to be added
 	And I click on "SEARCH" button
 	And I assert the migrated legal entity is fetched and shown in the result grid
 	
  Scenario: Validate the behaviour of fields in "Associated Parties" screen and to validate if a legal entity record can be searched  using "T24 CIF ID" data field to add as associated party 
  #Precondition - Associated Party1 to be added has "T24 CIF ID"  
  #Note - Continue from the previous scenario 
 	And I click on button "Add Selected"
 	And I am redirected to "Capture Hierarchy Details" screen
 	And I assert the migrated entity is visible in the hierarchy
 	And I click actions (...) button from the legal entity 
 	And I click "Add Associated Party" link
 	And I am redirected to "Associated Parties" screen #To be checked during execution
 	And I validate the following fields under "Selected Entity" section
   | Label                   | FieldType        | Visible | Editable | Mandatory | Field Defaults To         		|
   | Add Associated Party to | Aplphanumeric    | true    | false    | true      | "Name of migrated entity" 		|
   | Legal Entity Type       | Aplphanumeric    | true    | false    | true      | "LE type of migrated entity" |
  And I validate only the following fields are seen under "Associated Party Details" section 
   | Label                   | FieldType        | Visible | Editable | Mandatory | Field Defaults To|
   | T24 CIF ID              | Aplphanumeric    | true    | true	   | false     | NA               |
   | Legal Entity ID         | Aplphanumeric    | true    | true     | false     | NA               |
   | Legal Entity Type       | Drop-down        | true    | true     | true  	   | Select...        |
   | Legal Entity Name       | Aplphanumeric    | true    | true     | true      | NA               |
   | Country of Incorporation| Drop-down        | true    | true     | true      | Select...        |
  #Verify Legal Entity Name field NOT accepts special characters. Test Data:Special characters !@ # $ % ^ & * ( )} { ] [\ |'; ;/,~ `
  And I fill the data for "Associated Parties" with key "Data1"
  #Verify Legal Entity Name field accepts lower case characters. Test Data: Lower case char a to z
  And I fill the data for "Associated Parties" with key "Data2"
  #Verify Legal Entity Name field accepts Upper case characters. Test Data: Lower case char A to Z
  And I fill the data for "Associated Parties" with key "Data3"
  #Verify Legal Entity Name field accepts numbers zero to nine. Test Data: 0 to 9
  And I fill the data for "Associated Parties" with key "Data4"
  #Verify Legal Entity Name field accepts .(period/full stop)
  And I fill the data for "Associated Parties" with key "Data5"
  #Verify Legal Entity Name field accepts Space ()
  And I fill the data for "Associated Parties" with key "Data6"
  #Verify Legal Entity Name field accepts valid characters combination of upper case, lower case, numeric o to 9, . (full stop), space
  And I fill the data for "Associated Parties" with key "Data7"
  #Verify Legal Entity Name field NOT accepts more than 255 characters. Test data : 256 characters
  And I fill the data for "Associated Parties" with key "Data8"
  #Verify Legal Entity Name field accepts less than 255 characters. Test data : 255 characters
  And I fill the data for "Associated Parties" with key "Data9"
  And I clear data for Legal Entity Name field
  And I provide value for "T24 CIF ID" field of the associated party
  And I click on "SEARCH" button
  And I assert associated party is fetched and shown in the result grid
  And I select the associated party from the result grid #using radio button
  And I click on "ADD SELECTED" button
  And I am redirected to "Association Details" screen of added associated party
   
  
  Scenario: Validate behaviour of fields in "Association Details" screen when an already existing entity is added as associated party
  #Note - Continue from the previous scenario 
	And I validate the only following fields in "Association Details" section
		|Fenergo Label Name					|Field Type					|Visible	|Editable	|Mandatory	|Field Defaults To 	|
		|Assoication Type 						|Drop-down					|Yes			|Yes			|Yes				|Select...  				|
		|Type of Control             |Drop-down					|Yes			|Yes			|Yes        |Select...          |
	And I validate LOVs of "Assoication Type" field
	#Refer Assoication PBI020 (FENERGOFAB-224) for LOV list	
	And I validate LOVs of "Type of Control" field
	#Refer Assoication PBI020 (FENERGOFAB-224) for LOV list	
	And I validate "Is BOD?" field is not visible
	And I validate "Is Greater than 5% Shareholder?" field is not visible
	And I validate "Shareholding %" field is not visible
	And I select "Director" for "Assoication Type" field
	And I validate "Is BOD?" field is visible
	And I validate the conditional field in "Association Details" section
		|Fenergo Label Name			|Field Type		|Visible	|Editable		|Mandatory	|Field Defaults To 	|
		|Is BOD?           			|Drop-down		|Yes			|Yes				|Yes				|Select...					|
	And I validate LOVs of "Is BOD?" field
		|Yes|
		|No |
	And I select "Yes" for "Is BOD?" field
	And I validate "Is Greater than 5% Shareholder?" field is visible
	And I validate the conditional field in "Association Details" section
		|Fenergo Label Name							|Field Type		|Visible	|Editable		|Mandatory	|Field Defaults To 	|
		|Is Greater than 5% Shareholder?	|Alphanumeric	|Yes			|Yes				|Yes				|NA									|
	And I select "Has Shareholder" for "Assoication Type" field
	And I validate "Shareholding %" field is visible
	#This field should be displayed below "Assocation Type" field
	And I validate the conditional field in "Association Details" section
		|Fenergo Label Name			|Field Type		|Visible	|Editable		|Mandatory	|Field Defaults To 	|
		|Shareholding %					|Numeric			|Yes			|Yes				|No					|NA									|  
	And I select "CEO" for "Assoication Type" field
  And I validate "Is BOD?" field is not visible
  And I validate "Is Greater than 5% Shareholder?" field is not visible
  And I validate "Shareholding %" field is not visible
  And I select "Non Executive Director" for "Assoication Type" field
  And I validate "Is BOD?" field is visible
  And I validate the conditional field in "Association Details" section
    | Fenergo Label Name | Field Type | Visible | Editable | Mandatory | Field Defaults To |
    | Is BOD?            | Drop-down  | Yes     | Yes      | Yes       | Select...         |
  And I select "No" for "Is BOD?" field
  And I validate "Is Greater than 5% Shareholder?" field is not visible
  And I select "Ultimate Beneficial Owner (UBO)" for "Assoication Type" field
  And I validate "Is BOD?" field is not visible
  And I select "Trustee" for "Assoication Type" field
  And I validate "Is BOD?" field is visible
  #Play around with different combination like above steps
  And I fill data for mandatory fields
  And I click on "SAVE AND ADD ANOTHER" button
  And I assert that the associated party is added in the hierarchy successfully
  
  Scenario: To validate if a legal entity record can be searched  using "Legal Entity Type" data field to add as associated party and to validate if an associated party has further associatons is getting added successfully and the DM user has a Hierarchy Manager (Tree view) for the migrated legal entity
  #Precondition 1 - LE Type Associated Party2 should be known
  #Precondition 2 - Associated Party2 should have further associations (1 level of child associations)
  #Note - Continue from the previous scenario 
  And I choose value for "Legal Entity ID" field as the LE type of the associated party2
  And I click on "SEARCH" button
  And I assert associated party is fetched and shown in the result grid
  And I select the associated party from the result grid #using radio button
  And I click on "ADD SELECTED" button
  And I am redirected to "Association Details" screen of added associated party
  And I fill data for mandatory fields
  And I click on "SAVE" button
  And I validate the Associated Party2 is displayed in the hierarchy with its child associations
  #The heriarchy tree should have 3 levels.