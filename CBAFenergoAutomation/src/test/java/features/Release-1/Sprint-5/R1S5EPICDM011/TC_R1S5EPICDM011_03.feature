#Test Case: TC_R1S5EPICDM011_03
#PBI: R1S5EPICDM011
#User Story ID: N/A
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: Ability to receive a reconcilation file from Fenergo

  Scenario: Verify generated data (DM LE having No data in sub-flows) in XML file exported through "DM reconciliation File" option on "DM Screen-3(Generate Reconciliation File)" screen
    Given I login to Fenergo Application with "SuperUser"
    #Creating a legal entity with legal entity role as Client/Counterparty
    When I create a New DM request with FABEntityType as "FI" and LegalEntityRole as "Client/Counterparty"
    When I navigate "CaptureLEdetails" task
  
    #Test-data: Enter values in all the fields for "CaptureLEdetails" task
    And I enter data in all the fields for "CaptureLEdetails" task
    
    #Test-data: Do not add record in "Anticipated Transactional Activity (Per Month)" sub-flow
    And I validate no record is added in "Anticipated Transactional Activity (Per Month)" sub-flow
    
    #Test-data: Do not add doc in "Documents" sub-flow
    And I validate no Document is added in Document Sub-flow
  
   #Test-data:Do not add record in "Products" sub-flow
    And I validate no Record is added in Products Sub-flow
    
    #Test-data:Do not add any record in Add Relationship
   	And I validate no Record is added in Relationship Sub-flow
    
    #Test-data:Do not Add record  in Addresses sub-flow
    And I validate no Record is added in Addresses Sub-flow
    
    #Test-data:Do not add any record in Contacts
    And I validate no Record is added in Contacts Sub-flow
    
    When I click on "DM reconciliation File"
    #Test-data: Select Start date and End date with a difference of 1 week in which above data has been created
    When I select Start date and End date
    Then I click on Generate button       

    #Test-data: Verify data in generated XML file 
    And I verify data in XML.
 		
 		
 		
 		
 		
 		
 		
 		
 		
 		
    
 
 
 
 
 