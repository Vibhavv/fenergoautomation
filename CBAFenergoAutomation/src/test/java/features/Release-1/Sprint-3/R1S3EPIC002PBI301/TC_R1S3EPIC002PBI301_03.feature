#Test Case: TC_R1S3EPIC002PBI301_03
#PBI: R1S3EPIC002PBI301
#User Story ID: OOTBF050
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1S3EPIC002PBI301_03
@Automation
  Scenario: Validate the address screen before and after implementation (No changes)
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    When I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    And I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    
    When I navigate to "EnrichKYCProfileGrid" task
    #Test-data: Address screen to come up with no changes.Displaying same as earlier (refer snaphot from designer)
    #And I validate that address screen is displaying as earlier
    And I navigate to "AddAddresss" screen
    And I take a screenshot
