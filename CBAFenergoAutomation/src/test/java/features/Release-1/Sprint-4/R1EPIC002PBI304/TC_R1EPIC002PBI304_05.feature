#Test Case: TC_R1EPIC002PBI304_05
#PBI: R1EPIC002PBI304
#User Story ID:
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC002PBI304_05


  Scenario: Validate the field behaviour in Enrich KYC Profile screen for BBG Client type
    Given I login to Fenergo Application with "RM:BBG"
    #Create entity with Client type = Business Banking Group
    When I complete "NewRequest" screen with key "BBG"
    And I navigate to "CaptureRequestDetailsFAB" task
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    #Validate Financial Institition Due Diligence section is hidden from Enrich KYC Profile screen
    And I validate the following fields in "Enrich KYC Profile" Sub Flow
      | Label                               | Visible |
      | Financial Institition Due Diligence | false   |
    #Validate 'Source of Any Other Income' is hidden from Source of Funds And Welath Details section of Enrich KYC Profile screen
    And I validate the following fields in "Enrich KYC Profile" Sub Flow
      | Label                      | Visible |
      | Source of Any Other Income | false   |
