#Test Case: TC_R1EPIC011PBI006_02
#PBI: R1EPIC011PBI006
#User Story ID: US098
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC011PBI006_02 

@Automation
Scenario: 
	Validate behaviour of below fields in Anticipated Transactional Activity (Per Month) section of Enrich KYC Profile screen 
	#Value of Projected Transactions (AED)
	#Number of Transactions
	#Type/Mode of Transactions
	#Currencies Involved
	#Countries Involved
	#Transaction Type	  
  #Create entity with Country of Incorporation = UAE and client type = Business Banking Group
  Given I login to Fenergo Application with "RM:BBG"
     When I create a new request with FABEntityType as "Business Banking Group (BBG)" and LegalEntityRole as "Client/Counterparty"
      And I complete "CaptureNewRequest" with Key "BBG" and below data 
      | Product | Relationship | 
      | C1      | C1           | 
      And I click on "Continue" button
  
     When I complete "ReviewRequest" task
     Then I store the "CaseId" from LE360
    #=Given I login to Fenergo Application with "OnboardingMaker"
     #=When I search for the "10146"
     When I navigate to "ValidateKYCandRegulatoryGrid" task
     When I complete "ValidateKYC" screen with key "BBG"
      And I click on "SaveandCompleteforValidateKYC" button
     
     When I navigate to "EnrichKYCProfileGrid" task
  #	Verify the below behaviour in Anticipated Transactional Activity (Per Month) section
  #	Validate the Currencies Involved and Countries Involved fields are multiselect
     When I navigate to "Anticipated Transactional Activity (Per Month) In EnrichKYC" link by clicking on "Plus" link under action 
      And I validate the following fields in "Anticipated Transactional Activity" Sub Flow 
      | Label                                 | FieldType           | Visible | ReadOnly | Mandatory | DefaultsTo | 
      | Value of Projected Transaction (AED)  | Numeric             | true    | false    | NA   		 | NA         | 
      | Number of Transactions                | Numeric             | true    | false    | NA    		 | NA         | 
      | Type/Mode of Transactions             | Alphanumeric        | true    | false    | NA    		 | NA         | 
      | Currencies Involved                   | MultiSelectDropdown | true    | false    | NA   	   | Select...  | 
      | Countries Involved                    | MultiSelectDropdown | true    | false    | NA   	   | Select...  | 
      | Transaction Type                      | Dropdown            | true    | false    | NA   	   | Select...  | 
  #Verify the lovs for "Transaction Type" field
     Then I verify "Transaction Type" drop-down values
     Then I verify "Countries Involved" drop-down values
  
  #	And I validate "Currencies Involved" and "Countries Involved" drop-down has following LOVs 
  #Refer Country lov 
  