#Test Case: TC_R1EPIC011PBI006_04
#PBI: R1EPIC011PBI006
#User Story ID: US073 (PRIMARY), US074 (PRIMARY)
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC011PBI006_04

  @To_be_automated
  Scenario: Validate behaviour of below fields in Source Of Funds And Wealth Details section of Enrich KYC Profile screen
    #Legal Entity Source of Funds
    #Legal Entity Source of Income & Wealth
    Given I login to Fenergo Application with "RM"
    #Create entity with Country of Incorporation = UAE and client type = Business Banking Group
    When I create a new request with FABEntityType as "BusinessBankingGroup" and LegalEntityRole as "Client/Counterparty"
    And I fill the data for "CaptureNewRequest" with key "C1"
    And I click on "Continue" button
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYCandRegulatoryFAB" task
    When I navigate to "EnrichKYCProfileGrid" task
    #Verify the below behaviour in Anticipated Transactional Activity (Per Month) section
    And I validate the following fields in "Source Of Funds And Wealth Details" section
      | Label                                  | FieldType | Visible | Editable | Mandatory | DefaultsTo |
      | Legal Entity Source of Funds           | String    | true    | true     | false     | Select...  |
      | Legal Entity Source of Income & Wealth | String    | true    | true     | false     | Select...  |
		#Validate 'Legal Entity Source of Funds' accepts 1000 characters
		#Validate 'Legal Entity Source of Funds' NOT accepts more than 1000 characters
		#Validate 'Legal Entity Source of Income & Wealth' accepts 1000 characters
		#Validate 'Legal Entity Source of Income & Wealth' NOT accepts more than 1000 characters
		