#Test Case: TC_R1EPIC011PBI008_02
#PBI: R1EPIC011PBI008
#User Story ID: BBG CORP0, CORP18
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1EPIC011PBI008_02
@Tobeautomated

Scenario: Verify the document Type corresponding to the document category  drop-down along with mandatory and Non-mandatory flag on "document details screen" for "KYC document Requirements" task :
	Given I login to Fenergo Application with "RM"
	#Creating a legal entity with "Client Entity Type" as "BusinessBankingGroup" and "Legal Entity Role" as "Client/Counterparty" and "Country of Incorporation" as "AE-UNITED ARAB EMIRATES"
  When I create new request with LegalEntityrole as "Client/Counterparty" and "ClientType" as "BusinessBankingGroup"
  When I navigate to "CaptureRequestDetailsFAB" task
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	When I login to Fenergo Application with "OnboardingMaker"
	When I search for "CaseID" 
  When I navigate to "ValidateKYCandRegulatoryGrid" task 
  When I complete "ValidateKYCandRegulatoryFAB" task 
  When I navigate to "EnrichKYCProfileGrid" task 
  When I complete "EnrichKYCProfileFAB" task 
  When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task  
	When I navigate to "KYCDocumentRequirementsGrid" task
	
	# Test-Data:Verify document Type corresponding to the document category drop-down along with mandatory and Non-mandatory flag
	And I Verify the document Type corresponding to the document category drop-down along with mandatory and Non-mandatory flag(refer PBI doc for reference)
	
	|Requirement (List of documents to be shown            |Doc Category|             Doc type|                          Mandatory|
  |on KYC Document Requirement Section)|
   
   | Account Opening Form | 															| AOF |                     Account Opening Form |         	   	False|
   |Swift Message MT 199/299|            																							|Swift Message MT 199/299|   				False|
   |KYC Form    |                        																							|KYC Form    |                      True| 
   | Annual Report|                                                                   | Annual Report|                    True|
   | Proof of Address (Client) |         																							| Proof of Address (Client) |       True|
	
	 | Passport | 										                   | Authorized Signatories |      Passport |         	                False|
   |Passport (Dual National|            																							|Passport (Dual Nationality) |   			False|
   |Emirates ID |                        																							|Emirates ID   |                      False| 
   | Residence Visa|                                                                  |Residence Visa|                      True|
   | GCC ID  |         																																|  GCC ID   |                         True|
	 |Diplomat ID |																																			|Diplomat ID |	                      False|
	
	 |Passport | 										                   | MISC |                          Passport |         	                False|
   |Passport (Dual National|            																							|Passport (Dual Nationality) |   			False|
   |Emirates ID |                        																							|Emirates ID   |                      False| 
   | Residence Visa|                                                                  |Residence Visa|                      Flase|
   | GCC ID  |         																																|  GCC ID   |                         False|
	 |Diplomat ID |																																			|Diplomat ID |	                      False|
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
