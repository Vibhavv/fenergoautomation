#Test Case: TC_R1EPIC01PBI001_15
#PBI: R1EPIC01PBI001
#User Story ID: US015
#Designed by: Sanjeet Singh
#Last Edited by: Anusha PS

Feature: Products Section- Grid View - Cancelled products


Scenario: Verify that the Cancelled Product is available in "ValidateKYCandRegulatory" and  "Enrich KYC Profile" screens for KYCMaker user  once the stage moved to review and sign off 
	Given I login to Fenergo Application with "RelationshipManager" 
	#Creating a legal entity with legal entity role as Client/Counterparty 
	When I create new request with ClientEntityType as "Corporate"  and ClientEntityRole as "Client/Counterparty" 
	When I add a product in capture request details page 
	And I complete "CaptureRequestDetails" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Then I login to Fenergo Application with "KYCManager" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYCandRegulatory" task 
	Then I navigate to "EnrichKYCProfileSection" task 
	When I cancel a product in EnrichKYCProfile Screen 
	Then I can see the Cancelled product is visible in the product grid with status as "Cancelled" 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "RiskAssessmentGrid" task 
	When I complete "RiskAssessment" task 
	Then I login to Fenergo Application with "Onboarding Maker" 
	Then I navigate to "ValidateKYCandRegulatory" task 
	Then I can see the Cancelled product is visible in the product grid 
	Then I navigate to "EnrichKYCProfile" task 
	Then I can see the Cancelled product is visible in the product grid 
	
	
	