#Test Case: TC_R1EPIC002PBI006_01
#PBI: R1EPIC002PBI006
#User Story ID: US132, US134, US135, US136, US137, US139
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed.
Feature: TC_R1EPIC002PBI006_01

  @TobeAutomated
  Scenario: Verify the field behaviour of below new fields added in KYC Conditions section of Validate KYC and Regulatory Data Screen
    #Stock Exchange Domicile Country
    #Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)?
    #Specify the prohibited Category
    #Have the exceptional approvals been obtained to on-board /retain the client ?
    #Is the Entity operating with Flexi Desk?
    #Is UAE Licensed
    Given I login to Fenergo Application with "RM"
    #Create entity with Country of Incorporation = UAE and client type = corporate
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    And I fill the data for "CaptureNewRequest" with key "C1"
    And I click on "Continue" button
    And I complete "CaptureRequestDetailsFAB" task
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    And I check that below data is available
      | FieldLabel                                                           | Field Type | Mandatory | Editable | Field Defaults to | Visible |
      | Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)? | Dropdown   | Yes       | Yes      | Select...         | Yes     |
      | Is the Entity operating with Flexi Desk?                             | Textbox    | No        | Yes      | Select...         | Yes     |
      | Is UAE Licensed                                                      | Auto       | No        | Yes      | True              | Yes     |
    #Verfiy the field 'Stock Exchange Domicile Country' is displayed when 'yes' is selected for the field 'Is this entity publicly listed?'
    #Test data: Is this entity publicly listed? = Yes
    And I fill the data for "ValidateKYCandRegulatoryFAB" with key "Data1"
    And I check that below data is available
      | FieldLabel                      | Field Type | Mandatory | Editable | Field Defaults to | Visible | Multiselect |
      | Stock Exchange Domicile Country | Dropdown   | Yes       | Yes      | Select...         | Yes     | Yes         |
    #Verfiy the field 'Stock Exchange Domicile Country' is NOT displayed when 'NO' is selected for the field 'Is this entity publicly listed?'
    #Test data: Is this entity publicly listed? = No
    And I fill the data for "ValidateKYCandRegulatoryFAB" with key "Data1"
    And I check that below data is available
      | FieldLabel                      | Field Type | Mandatory | Editable | Field Defaults to | Visible | Multiselect |
      | Stock Exchange Domicile Country | Dropdown   | No        | NO       |                   | NO      | NO          |
    #Verfiy the field 'Stock Exchange Domicile Country' is displayed when 'yes' is selected for the field 'If is this entity parent publicly listed?'
    #Test data: If is this entity parent publicly listed? = Yes
    And I fill the data for "ValidateKYCandRegulatoryFAB" with key "Data1"
    And I check that below data is available
      | FieldLabel                      | Field Type | Mandatory | Editable | Field Defaults to | Visible | Multiselect |
      | Stock Exchange Domicile Country | Dropdown   | Yes       | Yes      | Select...         | Yes     | Yes         |
    #Verfiy the field 'Stock Exchange Domicile Country' is NOT displayed when 'No' is selected for the field 'If is this entity parent publicly listed?'
    #Test data: If is this entity parent publicly listed? = No
    And I fill the data for "ValidateKYCandRegulatoryFAB" with key "Data1"
    And I check that below data is available
      | FieldLabel                      | Field Type | Mandatory | Editable | Field Defaults to | Visible | Multiselect |
      | Stock Exchange Domicile Country | Dropdown   |           |          |                   | No      | No          |
    #Verfiy the field 'Stock Exchange Domicile Country' is NOT displayed when 'No' is selected for the fields 'If is this entity parent publicly listed?' and Is this entity publicly listed?
    #Test data: If is this entity parent publicly listed? = No and Is this entity publicly listed? = NO
    And I fill the data for "ValidateKYCandRegulatoryFAB" with key "Data1"
    And I check that below data is available
      | FieldLabel                      | Field Type | Mandatory | Editable | Field Defaults to | Visible | Multiselect |
      | Stock Exchange Domicile Country | Dropdown   |           |          |                   | No      | No          |
    #Verfiy the field 'Specify the prohibited Category' is displayed when 'yes' is selected for the field 'Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)?'
    #Verfiy the field 'Have the exceptional approvals been obtained to on-board /retain the client ?' is displayed when 'yes' is selected for the field 'Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)?'
    #Test data: Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)? = Yes
    And I fill the data for "ValidateKYCandRegulatoryFAB" with key "Data1"
    And I check that below data is available
      | FieldLabel                                                                    | Field Type | Mandatory | Editable | Field Defaults to | Visible |
      | Specify the prohibited Category                                               | Textbox    | Yes       | Yes      |                   | Yes     |
      | Have the exceptional approvals been obtained to on-board /retain the client ? | Dropdown   | Yes       | Yes      |                   | Yes     |
    #Verfiy 'Save and Complete' button is enabled when 'Yes' is selected for the field 'Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)?'
    #Test data: Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)? = NO
    And I fill the data for "ValidateKYCandRegulatoryFAB" with key "Data2"
    And I check that below data is available
      | FieldLabel        | Enabled |
      | SAVE AND COMPLETE | Yes     |
    #Verfiy the field 'Specify the prohibited Category' is not displayed when 'NO' is selected for the field 'Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)?'
    #Verfiy the field 'Have the exceptional approvals been obtained to on-board /retain the client ?' is non-mandatory when 'NO' is selected for the field 'Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)?'
    #Test data: Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)? = NO
    And I fill the data for "ValidateKYCandRegulatoryFAB" with key "Data2"
    And I check that below data is available
      | FieldLabel                                                                    | Field Type | Mandatory | Editable | Field Defaults to | Visible |
      | Specify the prohibited Category                                               | Textbox    | NO        | NO       |                   | No      |
      | Have the exceptional approvals been obtained to on-board /retain the client ? | Dropdown   | No        | No       |                   | Yes     |
    #Verfiy 'Save and Complete' button is disabled when 'NO' is selected for the field 'Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)?'
    #Test data: Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)? = NO
    And I fill the data for "ValidateKYCandRegulatoryFAB" with key "Data2"
    And I check that below data is available
      | FieldLabel        | Enabled |
      | SAVE AND COMPLETE | No      |
    #Verfiy 'save for later' button is enabled and able to save the data when 'NO' is selected for the field 'Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)?'
    #Test data: Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)? = NO
    And I fill the data for "ValidateKYCandRegulatoryFAB" with key "Data2"
    And I check that below data is available
      | FieldLabel     | Enabled |
      | SAVE FOR LATER | yES     |
    When I complete "ValidateKYCandRegulatoryFAB" task with Saveforlater button
    #Validate the 'Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)?' lovs
    Then I validate the specific LOVs for "Is this a Prohibited client (as per FAB�s AML/CTF/Sanctions Policy)?"
      | Lovs |
      | Yes  |
      | No   |
    #Validate the 'Has the exceptional approvals been obtained to on-board/retain the client? ' lovs
    Then I validate the specific LOVs for "Has the exceptional approvals been obtained to on-board/retain the client? "
      | Lovs |
      | Yes  |
      | No   |
    #Validate the 'Is the Entity operating with Flexi Desk?' lovs
    Then I validate the specific LOVs for "Is the Entity operating with Flexi Desk?"
      | Lovs |
      | Yes  |
      | No   |
    #Validate the 'Is UAE Licensed' lovs
    Then I validate the specific LOVs for "Is UAE Licensed"
      | Lovs  |
      | True  |
      | False |
    #Validate the 'Stock Exchange Domicile Country' lovs (Refer PBI - Countries )
    And I validate the LOV of "Stock Exchange Domicile Country" with key "lov1"
