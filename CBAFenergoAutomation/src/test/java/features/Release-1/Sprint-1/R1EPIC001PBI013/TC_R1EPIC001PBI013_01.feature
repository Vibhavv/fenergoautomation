#Test Case: TC_R1EPIC001PBI013_01
#PBI: R1EPIC001PBI013
#User Story ID: US034
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC001PBI013_01 - LE360-overview screen-"Anticipated Activity of Account" field
@Automation
  Scenario: 
    Verify "Anticipated Activity of Account" field is not visible under "Customer details" section of "LE360-overview" stage for RM user.
    
    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    
    When I navigate to "LE360overview" screen
    Then I check that below data is not visible
      | FieldLabel                      |
      | Anticipated Activity of Account |
    And I take a screenshot
