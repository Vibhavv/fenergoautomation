#Test Case: TC_R1EPIC001PBI013_03
#PBI: R1EPIC001PBI013
#User Story ID: US034
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC001PBI013_03 - LE verified details screen-"Anticipated Activity of Account" field

  @Automation
  Scenario: 
    Verify "Anticipated Activity of Account" field is not visible under "Customer details" section of "Enrich client Profile screen" stage for onboarding maker.

    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddress" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    #=================================================
    #=Validating that the case status is Open
    And I assert that the CaseStatus is "Open"
    
    Given I login to Fenergo Application with "SuperUser"
    When I search for the "CaseId"
    Then I navigate to "LE360overview" screen
    Then I navigate to "LEDetails" screen
    #Test Data:Verify "Anticipated Activity of Account" field is not visible under "Customer details" section
    Then I check that below data is not visible
      | FieldLabel                      |
      | Anticipated Activity of Account |
		And I take a screenshot      
    When I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I navigate to "LE360overview" screen
    Then I navigate to "LEDetails" screen
    #Test Data: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section
    Then I check that below data is not visible
      | FieldLabel                      |
      | Anticipated Activity of Account |
    And I take a screenshot
    When I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    Then I navigate to "LE360overview" screen
    Then I navigate to "LEDetails" screen
    #Test Data: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section
    Then I check that below data is not visible
      | FieldLabel                      |
      | Anticipated Activity of Account |
    And I take a screenshot  
    When I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    Then I navigate to "LE360overview" screen
    Then I navigate to "LEDetails" screen
    #Test Data: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section
    Then I check that below data is not visible
      | FieldLabel                      |
      | Anticipated Activity of Account |
    And I take a screenshot  
    When I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    Then I navigate to "LE360overview" screen
    Then I navigate to "LEDetails" screen
    #Test Data: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section
    Then I check that below data is not visible
      | FieldLabel                      |
      | Anticipated Activity of Account |
    And I take a screenshot  
    #When I login to Fenergo Application with "FlodVP"
    When I login to Fenergo Application with "CIB R&C KYC APPROVER - VP"
    When I search for the "CaseId"
    Then I navigate to "LE360overview" screen
    Then I navigate to "LEDetails" screen
    #Test Data: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section
    Then I check that below data is not visible
      | FieldLabel                      |
      | Anticipated Activity of Account |
    And I take a screenshot  
    When I login to Fenergo Application with "SVP"
    When I search for the "CaseId"
    Then I navigate to "LE360overview" screen
    Then I navigate to "LEDetails" screen
    #Test Data: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section
    Then I check that below data is not visible
      | FieldLabel                      |
      | Anticipated Activity of Account |
    And I take a screenshot  
    And I take a screenshot
    When I login to Fenergo Application with "BH:Corporate"
    When I search for the "CaseId"
    Then I navigate to "LE360overview" screen
    Then I navigate to "LEDetails" screen
    #Test Data: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section
    Then I check that below data is not visible
      | FieldLabel                      |
      | Anticipated Activity of Account |
    And I take a screenshot  
    When I login to Fenergo Application with "Group Compliance (CDD)"
    When I search for the "CaseId"
    Then I navigate to "LE360overview" screen
    Then I navigate to "LEDetails" screen
    #Test Data: Verify "Anticipated Activity of Account" field is not visible under "Customer details" section
    Then I check that below data is not visible
      | FieldLabel                      |
      | Anticipated Activity of Account |
    And I take a screenshot  
