#Test Case: TC_R1EPIC002PBI021_01
#PBI: R1EPIC002PBI021
#User Story ID: US015/16, US123, US125
#Designed by: Anusha PS
#Last Edited by: Anusha PS
#Test Case: TC_R1EPIC002PBI021_01
#PBI: R1EPIC002PBI021
#User Story ID: US015/16, US123, US125
#Designed by: Anusha PS
#Last Edited by: Anusha PS
#Test Case: TC_R1EPIC002PBI021_01
#PBI: R1EPIC002PBI021
#User Story ID: US015/16, US123, US125
#Designed by: Anusha PS
#Last Edited by: Anusha PS
#Test Case: TC_R1EPIC002PBI021_01
#PBI: R1EPIC002PBI021
#User Story ID: US015/16, US123, US125
#Designed by: Anusha PS
#Last Edited by: Anusha PS
@To_be_automated 
Feature: Capture Request >Address Section 

Scenario: 
	Validate if RM is able to see non-mandatory "Address" subflow in "Capture Request Details" and "Review Request" screens between "Relationships" and "Contact" panels & 
	Validate "Address" subflow in "Capture Request Details" screen is non-mandatory and has all the fields / behavior as per DD (Label Name/Sequence/Field Type/Visible/Editable/Mandatory/Field Defaults To)					

	Given I login to Fenergo Application with "RM" 
	
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty" 
	
	And I assert that "Address" subflow is visible in "Capture Request Details" screen 
	
	And I assert that "AddressesGrid" is non-mondatory 
	When I navigate to "Addresses" screen by clicking on "Plus" button from "CaptureRequestDetails" 
	
	And I validate the following fields in "Address" Sub Flow 
		|Label			|FieldType							|Visible	    |ReadOnly	|Mandatory	|DefaultsTo 	|
		|Address Type   |MultiSelectDropdown				|true			|false		|true		|Select...          |
		|Address Line 1 |Alphanumeric						|true			|false		|true		|NA                 |
		|Address Line 2	|Alphanumeric						|true			|false		|false		|NA                	|
		|Town / City	|Alphanumeric      			        |true			|false		|false		|NA               	|
		|ZIP Code 		|Alphanumeric      			        |true			|false		|false		|NA               	|
		|PO Box  		|Alphanumeric         	            |true			|false		|false      |NA             	|
		|Country  		|Dropdown            	            |true			|false		|true		|Select...         	|
		|State / Emirate|Dropdown							|true			|false      |false      |Select...         	|
	Then I complete "Addresses" screen with key "CountryAsUAE" 
	And I check that below data is mandatory 
		|FieldLabel|
		|PO Box|
		|State / Emirate| 
	Then I verify "State / Emirate" drop-down values 
	
	Then I complete "Addresses" screen with key "CountryAsIndia" 
	And I check that below data is non mandatory 
		|FieldLabel|
		|PO Box|
		|State / Emirate|
		
	And I check that "State / Emirate" is readonly 
	And I click on "SaveButton" button 
	Then I complete "CaptureNewRequest" screen with key "C!" 
	And I click on "Continue" button 
	And I assert that "Address" subflow is read only 
	
	
	#	
