#Test Case: TC_R1EPIC002PBI015_02
#PBI: R1EPIC002PBI015
#User Story ID: US112
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC002PBI015_02 

@Automation
Scenario: Validate "FAB Entity Type" label is renamed as "Client Type" label on "New request" stage 
	Given I login to Fenergo Application with "RM:IBG-DNE"
	When I click on "PlusButton" from "CompleteRequest" to create "NewRequestButton"
	Then I can see "FabEntityType" label is renamed as "ClientType" on EnterEntitydetails screen
	