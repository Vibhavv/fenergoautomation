#Test Case: TC_R1EPIC002PBI001_02
#PBI: R1EPIC002PBI001
#User Story ID: US 67a/b
#Designed by: Anusha PS
#Last Edited by: Anusha PS

@To_be_automated

Feature: Screening

Scenario: Validate if "Screening Decision" panel is not visible in Complete AML task - Assessment - Fircosoft Screening				

	Given I login to Fenergo Application with "RM" 
	#Creating a legal entity with "Client Entity Type" as "Corporate" and "Legal Entity Role" as "Client/Counterparty "
	When I create new request with LegalEntityrole as "Client/Counterparty"
	And I complete "CaptureRequestDetailsFAB" task 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
    When I navigate to "ValidateKYCandRegulatoryGrid" task as "OnboardingManager"
    When I complete "ValidateKYCandRegulatoryFAB" task 
    When I navigate to "EnrichKYCProfileGrid" task 
    When I complete "EnrichKYCProfileFAB" task 
    When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	#Add Fircosoft Screening by right clicking the legal entity from hierarchy and clicking "Add Fircosoft Screening"
	And I add Fircosoft Screening for the entity 
	#To perform below step, scroll down the screen and click "Edit" from the Actions (...) of the LE in the "Assessments" section
	And I navigate to "Assessment" screen
	#To perform below step, click "Edit" from the Actions (...) in the "Active Screenings" section
	And I navigate to "Fircosoft Screening" screen
	And I validate "Screening Decision" is not visible between "Fircosoft Screening Summary" and "Documents" section
	#Refer Screen Mock Up - New tab in PBI