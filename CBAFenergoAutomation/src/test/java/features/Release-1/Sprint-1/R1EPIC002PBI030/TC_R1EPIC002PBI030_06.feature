#Test Case: TC_R1EPIC002PBI030_06
#PBI: R1EPIC002PBI030
#User Story ID: OOTBF037, OOTBF038, OOTBF039
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R1EPIC002PBI030_06

  @Automation
  Scenario: Validate below mentioned fields display as hidden under "Business details" section for Onboarding Maker user
    Given I login to Fenergo Application with "RM"
    When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    When I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
     #Test data- Validate below mentioned fields display as hidden under "Business details" section
    Then I check that below data is not visible
      | FieldLabel                   |
      | Payment Countries            |
      | National Government          |
      | Name of Home State Authority |
    When I complete "AddAddress" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    When I navigate to "CaptureRiskCategoryGrid" task
    When I complete "RiskAssessment" screen with key "Low"
    
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task   
    
    #Validating that the case status is closed
    When I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId" 
    When I navigate to "LE360overview" screen
    When I navigate to "LEDetails" screen
    And I click on "Business Details" button
    Then I check that below data is not visible
      | FieldLabel                   |
      | Payment Countries            |
      | National Government          |
      | Name of Home State Authority |
    
    When I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"  
    When I navigate to "LE360overview" screen
    When I navigate to "VerifiedLEDetails" screen
    And I click on "Business Details" button
    Then I check that below data is not visible
      | FieldLabel                   |
      | Payment Countries            |
      | National Government          |
      | Name of Home State Authority |
    
    
    #==>Below steps were written for Manual Testing and is commented to have it as a reference.
    #When I navigate to "Business Details" section by clicking on "LEdetails" grid
    # Test data- Validate below mentioned fields display as hidden under "Business details" section "LE360-overview" task
    #Then I can see below mentioned fields display as hidden under "Business details" section
      #| Payment Countries            |
      #| National Government          |
      #| Name of Home State Authority |
    #When I navigate to "LEVerifieddetails" task by navigating through "LE360-overview" stage
    #When I navigate to "Businessdetails" section
    # Test data- Validate below mentioned fields display as hidden under "Business details" section on "LEVerifieddetails" task
    #Then I can see below mentioned fields display as hidden under "Business details" section
      #| Payment Countries            |
      #| National Government          |
      #| Name of Home State Authority |
