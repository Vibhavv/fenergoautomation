#Test Case: TC_R1EPIC002PBI011_13
#PBI: R1EPIC002PBI011
#User Story ID: US107
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: Enrich KYC profile task -Identifier Sub flow - Country of Tax Residency 

@TC_R1EPIC002PBI011_13
Scenario: Validate LOVs for "Country" field in Capture Request Details screen
	Given I login to Fenergo Application with "RM"
	When I create a new request with FABEntityType as "Corporate" and LegalEntityRole as "Client/Counterparty"
	Then I navigate to "TaxIdentifier" screen by clicking on "Plus" button from "CaptureNewRequest"
	Then I verify "Country" drop-down values
	#Refer PBI for the list
