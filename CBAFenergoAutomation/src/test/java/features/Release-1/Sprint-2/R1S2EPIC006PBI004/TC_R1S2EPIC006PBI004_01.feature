#Test Case: TC_R1S2EPIC006PBI004_01
#PBI: R1S2EPIC006PBI004
#User Story ID: US26
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: TC_R1S2EPIC006PBI004_01-Screening

  @Automation
  Scenario: 
    Validate if "Screening Decision" panel is not visible in Complete AML task - Assessment - Fircosoft Screening	and validate if "Onboarding Maker" is able to add documents and comments

    Given I login to Fenergo Application with "RM:IBG-DNE"
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddress" task
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I complete "CaptureHierarchyDetails" task
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task
    #=And I assert only the following system-calculated fields are present under "Confirmed Matches" section in the mentioned order
    Then I check that below data is visible
      | FieldLabel              |
      | Sanctions               |
      | PEP                     |
      | Adverse Media           |
      | FAB Internal Watch List |
    #=Add Fircosoft Screening by right clicking the legal entity from hierarchy and clicking "Add Fircosoft Screening"
    When I Initiate "Fircosoft" by rightclicking
    #=To perform below step, scroll down the screen and click "Edit" from the Actions (...) of the LE in the "Assessments" section
    #=And I complete "Fircosoft" from assessment grid
    And I click on edit button of added screening
    And I navigate and complete added screening
    #=And I validate "Assessment Decision" is not visible between "Active Screenings" and "Screening Tasks" section
    Then I check that below data is not visible
      | FieldLabel          |
      | Assessment Decision |
    #=And I click on "SaveandCompleteforAssessmentScreen1" button
