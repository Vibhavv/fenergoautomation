#Test Case: TC_R2EPIC018PBI006_08 
#PBI:R2EPIC018PBI006
#User Story ID:USTAX-020
#Designed by: Priyanka Arora
#Last Edited by: 
Feature: TC_R2EPIC018PBI006_08 
Scenario:
	#Validate for KYC Maker  field  "Form W-9 - Exemption from FATCA Reporting Code" becomes visible (Visible, editable, mandatory and defaults to as per DD) when "Tax Form Type" = CRS & FATCA CRS & FATCA CRS & FATCA CRS & FATCA Self-Certification Form" on "complete Tax U.S. classification screen" for 
	#Classification stage for Client type 'Corporate' for Maintenance workflow. (Validate the same while adding 'Related Party Company' on capture Hierarchy task screen)

	#Pre-requisite: Create a new LE with Client type 'Corporate' and complete the case
    #Initiate LEM case
    When I initiate "Legal Entity Maintenance" from action button
    #Select Area as 'LEdetails' and "Associated parties" and Area of change as 'Regulatory Updates'
    When I add AssociatedParty 'Tax Related Party Individual'and "Non-Individual" by right clicking  and source as 'US Tax'
    When I navigate to "CaptureProposedChangesGrid" task is generated 
    When I navigate to "SelectClassifications" task
    When I select "US TAX" from drop-down field "Select Classification to trigger"    
    When I click on "SaveandComplete" button        
    When I navigate to "USTaxClassification" task
    Then I select "CRS & FATCA Self-Certification Form" in "Tax Form Type" drop-down 
	And I validate field "Form W-9 - Exemption from FATCA Reporting Code" becomes visible 
	#Validate firld "Form W-9 - Exemption from FATCA Reporting Code" is displaying as below:
		| Label                        					   | Field Type    | Visible | Editable | Mandatory 			| Field Defaults To |
		| Form W-9 - Exemption from FATCA Reporting Code   | drop-down     | Yes     | Yes      | conditional mandatory |  Select...        |
	When I select "U.S. Person Exempt from Reporting" in "Chapter 4 FATCA Status field" drop-down 
	Then I validate field "Form W-9 - Exemption from FATCA Reporting Code" becomes mandatory 
	When I expand 'Taxrelatedparties' sub-flow
	When I navigate to the added record by clicking on 'Edit' button
	When I navigate to 'TaxRelatedPartyDetailsgrid' task		
	When I Expand 'USTaxClassifications' sub-flow	
	When I Click on plus button from the options button to add "Tax related party-company"
	Then I select "CRS & FATCA Self-Certification Form" in "Tax Form Type" drop-down 
	And I validate field "Form W-9 - Exemption from FATCA Reporting Code" becomes visible 
	#Validate firld "Form W-9 - Exemption from FATCA Reporting Code" is displaying as below:
		| Label                        					   | Field Type    | Visible | Editable | Mandatory 			| Field Defaults To |
		| Form W-9 - Exemption from FATCA Reporting Code   | drop-down     | Yes     | Yes      | conditional mandatory |  Select...        |				
	When I select "U.S. Person Exempt from Reporting" in "Chapter 4 FATCA Status field" drop-down 
	Then I validate field "Form W-9 - Exemption from FATCA Reporting Code" becomes mandatory
  	And I complete "Classification" task
    
    