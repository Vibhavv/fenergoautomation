Feature: LEM - Regulatory Update

  #Test Case: TC_R2EPIC018PBI008_06
  #PBI: R2EPIC018PBI008
  #User Story ID: USTAX-011b, USTAX-006
  #Designed by: Jagdev Singh
  #Last Edited by: Jagdev Singh
  @LEM
  Scenario: FI Client - Verify Regulatory Updates case can never be triggered automatically irrespective of the date in the field "Date of Expiration" on US tax classification
  
    #All the tasks in the RU are assigned to role group - Account Services.
    #Verify there is only one checker task in RU at stage-3 i.e.. Onboarding Review.
    #Verify there is no prelim tax classification triggered in any stage of RU.
    #RR is only available in LEM(LE Details/Area LOV), if we have US tax classifications are triggered in COB.
	#Verify only 1-Option i.e. US tax is avaialable in Select classification stage and these options too will based on classications triggered in COB.
	#Regulatory Updates case can never be triggered automatically irrespective of the date of field "Classification Review" Date on US tax classification
	#Regulatory Updates case can never be triggered automatically irrespective of the date in the field "Date of Expiration" on US tax classification

	#######################################################################################
	Precondition: Create COB with Client Type = FI Client, Confidential = FI and Booking Country = Bank-UAE Branch with follwing Data.
	##On Enrich KYC profile sceen -> Counter Party Type is PARTNERSHIP and Legal Constitution Type Partnership
	##On prelim- tax assesment page -> Do you have a US Tax Form from the client? as Yes
    
    #######################################################################################
    Given I login to Fenergo Application with "RM:CBG"
    When I complete "NewRequest" screen with key "C1"
    And I complete "CaptureNewRequest" with Key "CBG" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: FI Client"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
	#Select values to trigger US Tax classifications
	Then I select fields Counterparty Type = PARTNERSHIP and Legal Constitution Type = Partnership
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
	#Complete PreliminaryTaxAssessment task.
	When I navigate to PreliminaryTaxAssessment task
	Then I select Do you have a US Tax Form from the client? as Yes
	Then I complete "PreliminaryTaxAssessment" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
	
	#Verify US TAX classification is triggered.
    When I navigate to case details page
    Then I Verify US Tax classification is triggered.
	When I navigate to USTaxclassificationtask
	#verify there is no date againt "Classification Review Date", since Classification Review Date is disabled.
	Then I verify there is no date/blank againt field "Classification Review Date" on US tax classification
	When I select "CRS & FATCA Self-Certification Form" in drop-down field - "Tax Form Type" and click on update.
	Then I navigate to Document "CRS & FATCA Self-Certification Form" on Document Requirements section and click on Actions button and click on Add Document
	#Verify there is a field "Date of Expiration" and set it's value in 90-days time frame to trigger RU
	Then I set the value of field "Date of Expiration" such way to meet 90-days time frame to trigger RU
	#Navigate "Date of Expiration" on US tax classification
    Then I complete US Tax classification task
    
    Then I login to Fenergo Application with "RM:CBG"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "BUH:CBG"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "KYCMaker: FI"
    When I search for the "CaseId"
    And I complete "Waiting for UID from GLCMS" task from Actions button
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    
    #LEM flow starts
    
    And I initiate "Maintenance Request" from action button
	#Verify "Regulatory Updates" workflow is not triggered automatically, even when the "Date of Expiration" was set to less than 90-days(to trigger RU) in COB.
	Then I verify Regulatory Update is not triggered Automatically
	#Verify "Regulatory Updates" is always triggred manually
    When I select "LE Details" for field "Area" and select "Regulatory Updates" in field "LE Details Changes"
    And I click on "Submit" button
	
    Then I see "SelectClassifications" task is generated
	#Validate Prelimtaxclassification task is not triggered
	#Validate Select Classifications is assigned to "Account service Maker"
	The I validate task "SelectClassifications" is assigned to "Account service Maker" team
		And I navigate to "SelectClassifications" task
		
	#Validate only US Tax classification is available to select in drop down field "Select Classifications to Trigger"
	    Then I validate only US Tax classification is available drop down field "Select Classifications to Trigger"
		When I choose US Tax classification in drop down field "Select Classifications to Trigger"
		When I complete "SelectClassifications" task
		
	#Validate user new stage i.e. Classifications is available
	#Validate user navigates to next task i.e. CompleteUSTaxClassification and task is assigned to "Account service Maker"
	    Then I navigate to CompleteUSTaxClassification task
		Then I validate CompleteUSTaxClassification is assigned to "Account service Maker" team
		When I complete "CompleteUSTaxClassification" task
		
	#Validate user new stage i.e. Onboarding Review is available	
	#Validate user navigates to next task i.e. Onboarding Review and task is assigned to "Account service Checker"
	    Then I navigate to OnboardingReview task
		Then I validate OnboardingReview is assigned to "Account service Checker" team
		Then I complete "OnboardingReview" task
	
	#I Validated Prelimtaxclassification task is not triggered in any stage of RU workflow
	#Regulatory Review case is closed.