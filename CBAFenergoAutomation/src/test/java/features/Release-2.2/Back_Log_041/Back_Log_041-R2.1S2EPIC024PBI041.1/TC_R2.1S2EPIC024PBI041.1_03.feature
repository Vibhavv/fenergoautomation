#Test Case: TC_R2.1S2EPIC024PBI041.1_03
#PBI: TC_R2.1S2EPIC024PBI041.1_03
#User Story ID: System Calculated - Compliance Review Date
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2.1S2EPIC024PBI041.1_03

Scenario: #Verify system should calculate the 'Compliance Review Date' as per 'Medium' Risk rating (3 years from the KYC Approval date) for Lite KYC Workflow on 'LE360- LEdetails' screen

	Login and Creating an Onboarding flow for an FAB application
	Given I login to Fenergo Application with "RM:FI" 
	When I complete "NewRequest" screen with key "LiteKYC-FI"  and Legal entity Type "Financial Institution - State Owned Entity 'SOE' listed on FAB Recognized Stock Exchange"
	And I complete "CaptureNewRequest" with Key "LiteKYC-FI" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	
	Given I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "LiteKYC-FI" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I complete "AddAddressFAB" task 
	
	When I complete "EnrichKYC" screen with key "LiteKYC-FI" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual" 
	When I complete "AssociationDetails" screen with key "Director" 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	Then I complete "KYCDocumentRequirements" task 
	
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
	Then I complete "CompleteAML" task 
	
	When I navigate to "CompleteID&VGrid" task 
	When I complete "ID&V" task 
	When I complete "EditID&V" task 
	When I complete "AddressAddition" in "Edit Verification" screen 
	When I complete "Documents" in "Edit Verification" screen 
	When I complete "TaxIdentifier" in "Edit Verification" screen 
	When I complete "LE Details" in "Edit Verification" screen 
	When I click on "SaveandCompleteforEditVerification" button 
	When I complete "CompleteID&V" task 
	
	When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessment" screen with key "LiteKYC" 
	
	Then I login to Fenergo Application with "RM:FI" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "BUH:FI" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "KYCMaker: FIG" 
	When I search for the "CaseId" 
	When I navigate to "CaptureFabReferencesGrid" task 
	When I complete "CaptureFABReferences" task 
	And I assert that the CaseStatus is "Closed" 
	
	#Verify system should calculate the 'Compliance Review Date' as per 'Medium' Risk rating (3 years from the KYC Approval date)
	When I navigate to LE360-LEdetails screen
	Then I verify 'Compliance Review Date' is displaying 
	And 'Compliance Review Date' is generated as 'KYC Approval Date + 3 years' (as per Medium RR)
	
	
	