Feature: LEM - KYC Data and Customer Details - Data Attributes Changes

  #Test Case: TC_R2EPIC018PBI002.3_06
  #PBI: R2EPIC018PBI002.3v0.2
  #User Story ID: NA
  #Designed by: Sasmita Pradhan
  #Last Edited by: Sasmita Pradhan
  @LEM
  Scenario: FI - Validate the behavior of "Update Customer Details " task when area is 'LE Details' and Le Details Changes is 'KYC Data and Customer details' during LEM :
    #Comments section - falsechange (OOTB feature)
    #Addresses section -  falsechange(OOTB feature)
    #'Business Markets' subflow should be hidden
    #'Financial Institution Due Diligence'subflow should be hidden
    #'Anticipated Transaction Volumes (Annual in AED)' section should be hidden
    #Field behavior in Customer Details section (8 fields should be hidden, 13 new fields should be added, 5 fields should be modified)
    #Field behavior in Business Details section (2 fields should be hidden, 9 new fields should be added, 2 fields should be modified)
    #Field behavior in Source Of Funds And Wealth Details section (2 field should be modified)
    #Field behavior in Industry Codes Details section (7 fields should be hidden, 1 new field should be added, 2 fields should be modified)
    #"Primary Industry of Operation" ,"Secondary Industry of Operation ","Primary Industry of Operation Islamic" and "Primary Industry of Operation UAE "field should be greyed out
    #Field behavior in Internal Booking Details section (4 fields should be hidden, 3 new fields should be added, 1 field should be modified)
    #New Section Anticipated Transactional Activity(Per Month) should be added on Update Customer Details Screen
    #Precondition: Create COB with Client Type = FI and country of incorporation = AE-UNITED ARAB EMIRATES and following data in "Validate KYC and Regulatory Data" task and case status should be closed
    ##Is this entity publicly listed? - No
    ##Is this a Prohibited client (as per FAB's AML/CTF/Sanctions Policy)?-true
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    #LEM flow starts
    #Initiate "Maintenance Request"
    And I initiate "Maintenance Request" from action button
    When I select "LE Details" for field "Area"
    When I select "KYC Data and Customer Details" for field "LE Details Changes"
    And I click on "CreateLEMSubmit" button
    Then I see "CaptureProposedChangesGrid" task is generated
    And I navigate to "CaptureProposedChangesGrid" task
    And I click on "SaveandCompleteCaptureProposedChanges" button
    Then I see "UpdateCustomerDetails" task is generated
    And I navigate to "UpdateCustomerDetails" task
    Then I check that below subflow is not visible
      | Subflow                                         |
      | Business Markets                                |
      | Financial Institution Due Diligence             |
      | Anticipated Transaction Volumes (Annual in AED) |
    Then I check that below subflow is visible
      | Subflow                                        |
      | Anticipated Transactional Activity (Per Month) |
    #Validate the behavior of "Update Customer Details " task when area is LE Details and Led Details Changes is 'KYC Data and Customer details'
    #And I assert "Business Markets",'Financial Institution Due Diligence'subflow and 'Anticipated Transaction Volumes (Annual in AED)' section is not visible on "Update Customer Details" screen
    #And I assert "Anticipated Transactional Activity(Per Month) section is visible on " Update Customer Details" screen
    ##Field behavior in Customer Details section (8 fields should be hidden, 13 new fields should be added, 5 fields should be modified)
    #And I validate the following fields in "Customer Details " section #Only the following fields should be present in the mentioned order
    And I validate the following fields in "Customer Details" Sub Flow
      | Label                                    | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
      | ID                                       | TextBox   | true    | false    | false     | NA         |
      | Client Type                              | Dropdown  | true    | false    | false     | NA         |
      | Legal Entity Name                        | TextBox   | true    | false    | false     | NA         |
      | Legal Entity Type                        | Dropdown  | true    | false    | false     | NA         |
      | Entity Type                              | Dropdown  | true    | false    | false     | NA         |
      | LEI                                      | TextBox   | true    | true     | false     | NA         |
      | Country of Incorporation  Establishment  | Dropdown  | true    | true     | true      | NA         |
      | Country of Domicile Physical Presence    | Dropdown  | true    | true     | true      | NA         |
      | Date of Incorporation  Establishment     | Date      | true    | true     | true      | NA         |
      | GLEIF Legal Name                         | TextBox   | true    | true     | false     | NA         |
      | GLEIF Legal Jurisdiction                 | TextBox   | true    | true     | false     | NA         |
      | GLEIF Legal Form                         | TextBox   | true    | true     | false     | NA         |
      | GLEIF Entity Status                      | TextBox   | true    | true     | false     | NA         |
      | Registration Number                      | TextBox   | true    | true     | false     | NA         |
      | Name of Registration Body                | TextBox   | true    | true     | true      | NA         |
      | Customer Relationship Status             | Dropdown  | true    | false    | false     | NA         |
      | Length of Relationship                   | Dropdown  | true    | true     | true      | NA         |
      | Does the entity have a previous name(s)? | Dropdown  | true    | true     | true      | true       |
      | Previous Name(s)                         | TextBox   | true    | true     | true      | NA         |
      | TradingOperation Name                    | TextBox   | true    | true     | false     | NA         |
      | Legal Entity Name (Parent)               | TextBox   | true    | true     | false     | NA         |
      | Website Address                          | TextBox   | true    | true     | false     | NA         |
      | Entity Level                             | Dropdown  | true    | true     | NA        | NA         |
      | Legal Counter party type                 | Dropdown  | true    | true     | true      | NA         |
      | Legal Constitution Type                  | Dropdown  | true    | true     | true      | NA         |
      | Emirate                                  | Dropdown  | true    | false    | true      | NA         |
      | SWIFT Address                            | TextBox   | true    | false    | true      | NA         |
      | Group Name                               | TextBox   | true    | true     | false     | NA         |
    #And I validate falsespecial characters / falselowercase characters allowed for field "Trading/Operation Name"and Legal Entity Name (Parent) type
    #And I validate date cannot be in the future for field Date of Incorporation
    And I fill "SpecialCharacters" data of length "10" in "Trading/Operation Name" field of "Update Customer Details" screen in "LEM" workflow
    And I take a screenshot
    And I fill "SpecialCharacters" data of length "10" in "Legal Entity Name (Parent)" field of "Update Customer Details" screen in "LEM" workflow
    And I take a screenshot
    And I fill "LowerCaseCharacters" data of length "10" in "Trading/Operation Name" field of "Update Customer Details" screen in "LEM" workflow
    And I take a screenshot
    And I fill "LowerCaseCharacters" data of length "10" in "Legal Entity Name (Parent)" field of "Update Customer Details" screen in "LEM" workflow
    And I take a screenshot
    When I select "01-01-2022" for "TextBox" field "Date of Incorporation"
    #Validate the below fields are not visible under "Customer Details" section on "Update Customer Details" screen
    And I check that below data is not visible
      | FieldLabel                               |
      | FAB Segment                              |
      | Legal Entity Category                    |
      | Anticipated Activity of Account          |
      | Primary Business Location                |
      | Residential Status                       |
      | Channel & Interface                      |
      | Can the corporation issue bearer shares? |
      | Legal Status                             |
    ##Field behavior in Business Details section (2 fields should be hidden, 9 new fields should be added, 2 fields should be modified)
    #And I validate the following fields in "Business Details" section #Only the following fields should be present in the mentioned order
    #| FieldLabel                                                | Field Type               | Visible | Editable    | Mandatory   | DefaultsTo        |
    And I validate the following fields in "Business Details" Sub Flow
      | Label                                              | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Nature of Business Activity                        | TextBox   | true    | true     | true      | NA         |
      | Primary Business Activity                          | TextBox   | true    | true     | false     | NA         |
      | Active Presence in Sanctioned CountriesTerritories | Dropdown  | true    | true     | false     | true       |
      | Specify the Sanctioned CountriesTerritories        | Dropdown  | true    | true     | false     | NA         |
      | Offshore Banking License                           | Dropdown  | true    | true     | false     | NA         |
    #Validate the below fields are not visible under "Business Details" section
    #And I Validate the below fields are not visible under "Business Details" section
    And I check that below data is not visible
      | FieldLabel                       |
      | Payment Countries                |
      | Anticipated Transactions Profile |
    ##Field behavior in Source Of Funds And Wealth Details section
    ##For Client Type = FI and Product = Call Account or Savings Account then field and panel should be visible, else panel should be hidden.
    #Only the following fields should be present in the mentioned order
    And I validate the following fields in "Source Of Funds And Wealth Details " Sub Flow
      | Label                        | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Legal Entity Source of Funds | Dropdown  | true    | false    | false     | NA         |
    ##Field behavior in Industry Codes Details section (7 fields should be hidden, 9 new fields should be added, 2 fields should be modified)
    #Only the following fields should be present in the mentioned order
    And I validate the following fields in "Industry Codes Details" Sub Flow
      | Label                                 | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Primary Industry of Operation         | Dropdown  | true    | true     | false     | NA         |
      | Secondary Industry of Operation       | Dropdown  | true    | true     | false     | NA         |
      | Primary Industry of Operation Islamic | NA        | true    | true     | false     | NA         |
      | Primary Industry of Operation UAE)    | Dropdown  | true    | true     | false     | NA         |
    ##Validate if above mentioned fields should be greyed out
    And I assert below fields are greyed out
      | FieldLabel                            |
      | Primary Industry of Operation         |
      | Secondary Industry of Operation       |
      | Primary Industry of Operation Islamic |
      | Primary Industry of Operation UAE)    |
    #Validate the below fields are not visible under "Industry Codes Details" section
    And I check that below data is not visible
      | FieldLabel          |
      | NAIC                |
      | ISIN                |
      | Secondary ISIC      |
      | NACE 2 Code         |
      | Stock Exchange Code |
      | Central Index Key   |
      | SWIFT BIC           |
    ##Field behavior in Internal Booking Details section (4 fields should be hidden, 3 new fields should be added, 1 field should be modified)
    #Only the following fields should be present in the mentioned order
    And I validate the following fields in "Internal Booking Details " Sub Flow 
      | FieldLabel                              | Field Type          | Visible | Readonly | Mandatory | DefaultsTo |
      | Request Type                            | Dropdown            | true    | true     | false     | NA         |
      | SRequest Origin                         | DatePicker          | true    | true     | false     | NA         |
      | Client Reference                        | TextBox             | true    | false    | false     | NA         |
      | Booking Country                         | Dropdown            | true    | false    | true      | NA         |
      | Consented to Data Sharing Jurisdictions | MultiSelectDropdown | true    | false    | false     | NA         |
      | Confidential                            | Dropdown            | true    | false    | true      | NA         |
      | Jurisdiction                            | NA                  | true    | true     | false     | NA         |
      | Sector Description                      | Dropdown            | true    | false    | false     | NA         |
      | UID Originating Branch                  | Dropdown            | true    | false    | true      | NA         |
      | Propagate To Target Systems             | TextBox             | true    | true     | true      | NA         |
    #Validate for client type "FI " UID origination branch should be defaulted to  Head Office _ BNK-100  and user will able to edit this field
    #Validate the below fields are not visible under "Internal Booking Details" section
    #And I Validate the below fields are not visible under "Internal Booking Details" section
    And I check that below data is not visible
      | FieldLabel        |
      | Priority          |
      | IFrom Office Area |
      | On Behalf Of      |
      | Internal Desk     |
