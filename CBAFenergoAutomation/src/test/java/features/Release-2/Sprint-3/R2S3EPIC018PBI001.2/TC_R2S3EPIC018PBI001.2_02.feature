#Test Case: TC_R2S3EPIC018PBI001.2_02
#PBI: R2S3EPIC018PBI001.2
#User Story ID: US13
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2S3EPIC018PBI001.2_02

  Scenario: Validate user is able to Cancel the LEM case at any 'Materiality Review' stage during the review process for LEM Workflow (Also, Validate 'Comments' sections display as mandatory on cancellation request screen)
    #Validate user is able to Cancel the LEM case at any 'Maintenance Request' stage during the review process for LEM Workflow (Also, Validate 'Comments' sections display as mandatory on cancellation request screen)
    #Validate user is able to Cancel the LEM case at any 'Risk Assessment' stage during the review process for LEM Workflow (Also, Validate 'Comments' sections display as mandatory on cancellation request screen)
    #Validate user is able to Cancel the LEM case at any 'review and Approval' stage during the review process for LEM Workflow (Also, Validate 'Comments' sections display as mandatory on cancellation request screen)
    When I complete "NewRequest" screen with key "Corporate"
    And I complete "CaptureNewRequest" with Key "C1" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    Then I store the "CaseId" from LE360
    When I complete "EnrichKYC" screen with key "C1"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    #	Then I compare the list of documents should be same as "COBDocument" for ClientType "Corporate"
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "ID&V" task
    When I complete "EditID&V" task
    When I complete "AddressAddition" in "Edit Verification" screen
    When I complete "Documents" in "Edit Verification" screen
    When I complete "TaxIdentifier" in "Edit Verification" screen
    When I complete "LE Details" in "Edit Verification" screen
    When I click on "SaveandCompleteforEditVerification" button
    When I complete "CompleteID&V" task
    When I navigate to "CompleteRiskAssessmentGrid" task
    When I complete "RiskAssessment" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "4027"
    When I navigate to "CaptureFabReferencesGrid" task
    When I complete "CaptureFABReferences" task
    And I assert that the CaseStatus is "Closed"
    #Initiate Legal Entity Maintenance case
    Given I login to Fenergo Application with "AccountServicemaker"
    When I navigate to "LE360Ledetails" task and click on Actions button
    When I click on "MaintenanceRequest" to initiate Legal Entity maintenance workflow
    When I navigate to "Maintenancerequest" task
    When I Select Area as "LEdetails", LE Details changes as "Associatedparties" and add "Reason" and click on 'Submit'
    When I navigate to "CaptureProposedchanges" task
    When I complete "CaptureProposedchanges" task
    When I navigate to "UpdateCustomerdetails" task
    When I complete "UpdateCustomerdetails" task
    When I navigate to "KYCDocumentRequirement" task
    When I Complete "KYCDocumentRequirement" task
    When I navigate to "Onboardingreview" task
    When I Complete "Onboardingreview" task
    Then I validate "Optionaltaskreview" task is generated in task grid
    When I navigate to "Casedetails" screen
    #Validate 'cancelcase' option is present on Actions button on "Casedetails" screen
    When I click on Actions button and select 'CancelCase' option
    #Validate user is navigate to Cancellation request screen and comments textbox is displaying as mandatory
    When I navigate to 'CancellationRequest' task screen
    Then I validate 'comments' textbox is displaying as mandatory
    When I select 'cancellationreason' and add 'comments' and click on 'Submit' button
    When I navigate to 'Cancellationchecklist' task screen
    When I check the checkbox on 'Cancellationchecklist' task screen and click on 'cancelcase' button
    #validate case is terminated and case status is updated as 'closed'
    #validate No new task is generated once the case is terminated
    Then I see Status in task grid is updated as 'Terminated' for "Optionaltaskreview" task
    And No new task is generated in the Task Grid
    And I assert case status is updated as 'Closed'
    #Validate user is able to Cancel the LEM case at any 'Maintenance Request' stage during the review process for LEM Workflow
    #(Also, Validate 'Comments' sections display as mandatory on cancellation request screen)
    #Initiate Legal Entity Maintenance case
    Given I login to Fenergo Application with "AccountServicemaker"
    When I navigate to "LE360Ledetails" task and click on Actions button
    When I click on "MaintenanceRequest" to initiate Legal Entity maintenance workflow
    When I navigate to "Maintenancerequest" task
    When I Select Area as "LEdetails", LE Details changes as "Associatedparties" and add "Reason" and click on 'Submit'
    When I navigate to "CaptureProposedchanges" task
    When I complete "CaptureProposedchanges" task
    When I navigate to "UpdateCustomerdetails" task
    When I navigate to "Casedetails" screen
    #Validate 'cancelcase' option is present on Actions button on "Casedetails" screen
    When I click on Actions button and select 'CancelCase' option
    #Validate user is navigate to Cancellation request screen and comments textbox is displaying as mandatory
    When I navigate to 'CancellationRequest' task screen
    Then I validate 'comments' textbox is displaying as mandatory
    When I select 'cancellationreason' and add 'comments' and click on 'Submit' button
    When I navigate to 'Cancellationchecklist' task screen
    When I check the checkbox on 'Cancellationchecklist' task screen and click on 'cancelcase' button
    #validate case is terminated and case status is updated as 'closed'
    #validate No new task is generated once the case is terminated
    Then I see Status in task grid is updated as 'Terminated' for "UpdateCustomerdetails" task
    And No new task is generated in the Task Grid
    And I assert case status is updated as 'Closed'
    #Validate user is able to Cancel the LEM case at any 'Risk Assessment' stage during the review process for LEM Workflow
    # (Also, Validate 'Comments' sections display as mandatory on cancellation request screen)
    Given I login to Fenergo Application with "AccountServicemaker"
    When I navigate to "LE360Ledetails" task and click on Actions button
    When I click on "MaintenanceRequest" to initiate Legal Entity maintenance workflow
    When I navigate to "Maintenancerequest" task
    When I Select Area as "LEdetails", LE Details changes as "Associatedparties" and add "Reason" and click on 'Submit'
    When I navigate to "CaptureProposedchanges" task
    When I complete "CaptureProposedchanges" task
    When I navigate to "UpdateCustomerdetails" task
    When I complete "UpdateCustomerdetails" task
    When I navigate to "KYCDocumentRequirement" task
    When I Complete "KYCDocumentRequirement" task
    When I navigate to "Onboardingreview" task
    When I Complete "Onboardingreview" task
    When I navigate to "Optionaltaskreview" task
    When I Complete "Optionaltaskreview" task
    Given I login to Fenergo Application with "KYCMaker"
    When I navigate to "CompleteAML" task
    When I Complete "CompleteAML" task
    When I navigate to "CompleteID&V" task
    When I Complete "CompleteID&V" task
    Then I validate "CompleteRiskAssessment" task is generated in task grid
    When I navigate to "Casedetails" screen
    #Validate 'cancelcase' option is present on Actions button on "Casedetails" screen
    When I click on Actions button and select 'CancelCase' option
    #Validate user is navigate to Cancellation request screen and comments textbox is displaying as mandatory
    When I navigate to 'CancellationRequest' task screen
    Then I validate 'comments' textbox is displaying as mandatory
    When I select 'cancellationreason' and add 'comments' and click on 'Submit' button
    When I navigate to 'Cancellationchecklist' task screen
    When I check the checkbox on 'Cancellationchecklist' task screen and click on 'cancelcase' button
    #validate case is terminated and case status is updated as 'closed'
    #validate No new task is generated once the case is terminated
    Then I see Status in task grid is updated as 'Terminated' for "CompleteRiskAssessment" task
    And No new task is generated in the Task Grid
    And I assert case status is updated as 'Closed'
    #Validate user is able to Cancel the LEM case at any 'review and Approval' stage during the review process for LEM Workflow
    #(Also, Validate 'Comments' sections display as mandatory on cancellation request screen)
    Given I login to Fenergo Application with "AccountServicemaker"
    When I navigate to "LE360Ledetails" task and click on Actions button
    When I click on "MaintenanceRequest" to initiate Legal Entity maintenance workflow
    When I navigate to "Maintenancerequest" task
    When I Select Area as "LEdetails", LE Details changes as "Associatedparties" and add "Reason" and click on 'Submit'
    When I navigate to "CaptureProposedchanges" task
    When I complete "CaptureProposedchanges" task
    When I navigate to "UpdateCustomerdetails" task
    When I complete "UpdateCustomerdetails" task
    When I navigate to "KYCDocumentRequirement" task
    When I Complete "KYCDocumentRequirement" task
    When I navigate to "Onboardingreview" task
    When I Complete "Onboardingreview" task
    When I navigate to "Optionaltaskreview" task
    When I Complete "Optionaltaskreview" task
    Given I login to Fenergo Application with "KYCMaker"
    When I navigate to "CompleteAML" task
    When I Complete "CompleteAML" task
    When I navigate to "CompleteID&V" task
    When I Complete "CompleteID&V" task
    When I navigate to "CompleteriskAssessment" task
    When I Complete "CompleteriskAssessment" task
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    Then I validate "CIBR&CKYCApproverAVPReviewGrid" task is generated in task grid
    When I navigate to "Casedetails" screen
    #Validate 'cancelcase' option is present on Actions button on "Casedetails" screen
    When I click on Actions button and select 'CancelCase' option
    #Validate user is navigate to Cancellation request screen and comments textbox is displaying as mandatory
    When I navigate to 'CancellationRequest' task screen
    Then I validate 'comments' textbox is displaying as mandatory
    When I select 'cancellationreason' and add 'comments' and click on 'Submit' button
    When I navigate to 'Cancellationchecklist' task screen
    When I check the checkbox on 'Cancellationchecklist' task screen and click on 'cancelcase' button
    #validate case is terminated and case status is updated as 'closed'
    #validate No new task is generated once the case is terminated
    Then I see Status in task grid is updated as 'Terminated' for "CIBR&CKYCApproverAVPReviewGrid" task
    And No new task is generated in the Task Grid
    And I assert case status is updated as 'Closed'
