#Test Case: TC_R1S2EPIC003PBI202_07
#PBI: R1S2EPIC003PBI202
#User Story ID: NA
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R1S2EPIC003PBI202_07

 @Automation
  Scenario: BBG-Verify the KYC Approver (KYC Manager) user is able to override the risk from Low to 'Medium' and verify the appropriate tasks are trigerred in Review and approval stage for Medium risk.
  #Additional Scenario: KYC Maker user assigns the Complete Risk Assessment task to KYC Manager
  #Precondition: Input the appropriate data to get Low Risk rating in Complete Risk Assessment screen
    Given I login to Fenergo Application with "RM:BBG"
    When I complete "NewRequest" screen with key "BBG"
    And I complete "CaptureNewRequest" with Key "LowRiskBBG" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "LowRiskBBG"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    When I complete "AddAddressFAB" task
    When I complete "EnrichKYC" screen with key "LowRiskBBG"
    And I click on "SaveandCompleteforEnrichKYC" button
    
    Given I login to Fenergo Application with "KYCMaker: BBG"
    When I search for the "CaseId"
    Then I store the "CaseId" from LE360
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    When I navigate to "KYCDocumentRequirementsGrid" task
    Then I store the "CaseId" from LE360
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "LowScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    
    When I navigate to "CompleteRiskAssessmentGrid" task 
		Then I verify the populated risk rating is "Low" 
		And I click on "Case Details" button
	  When I assign the task "Complete Risk Assessment" to role group "CIB R&C KYC APPROVER - KYC Manager" and user name "LastName, KYCManager"
		Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
		When I search for the "CaseId"
		Then I store the "CaseId" from LE360 
		When I navigate to "CompleteRiskAssessmentGrid" task
		Then I verify the populated risk rating is "Low"
		Then I override risk to "Medium" 
		Then I verify the populated risk rating is "Medium"
		When I complete "RiskAssessment" task 

	Then I login to Fenergo Application with "RM:BBG" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "BUH:BBG" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId"
	And I complete "Waiting for UID from GLCMS" task from Actions button 
	When I navigate to "CaptureFabReferencesGrid" task 
	When I complete "CaptureFABReferences" task 
	And I assert that the CaseStatus is "Closed" 
	