#Test Case: TC_R2S2EPIC003PBI204_01
#PBI: R2S2EPIC003PBI204
#User Story ID: NA
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R2S2EPIC003PBI204_01

  @Automation
  Scenario: COB-Validate the LOVs for the fields 'Primary Industry of Operation', 'Secondary Industry of operation' and 'Primary Industry of Operation-UAE' in Enrich KYC Profile screen of COB flow
    #Additional Scenario: Existing lov '00005-Individual and Trusts' is renamed as '00005-Trusts
    #Existing Lovs:547, New Lovs:79. Total Lovs: 626
    #Additional Scenario: Refer back and check the values are retained for the three fields
    #Additional Scenario: Verify the values are updated correctly in LE360-LEdetails screen
    #Additional Scenario: Verify the values are updated correctly in Verified LE details of LE360 screen
    
    Given I login to Fenergo Application with "RM:IBG-DNE" 
		When I complete "NewRequest" screen with key "Corporate" 
		And I complete "CaptureNewRequest" with Key "C1" and below data 
			| Product | Relationship |
			| C1      | C1           |
		And I click on "Continue" button 
		When I complete "ReviewRequest" task 
		Then I store the "CaseId" from LE360 
		
		Given I login to Fenergo Application with "KYCMaker: Corporate" 
		When I search for the "CaseId" 
		When I navigate to "ValidateKYCandRegulatoryGrid" task 
		When I complete "ValidateKYC" screen with key "C1" 
		And I click on "SaveandCompleteforValidateKYC" button 
		When I navigate to "EnrichKYCProfileGrid" task 
		When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
		When I complete "AddAddressFAB" task 
	
    And I verify "Primary Industry of Operation" drop-down values
    And I verify "Primary Industry of Operation UAE" drop-down values
    And I verify "Secondary Industry of Operation" drop-down values
		When I complete "EnrichKYC" screen with key "C1"

    And I click on "SaveForLaterEnrichKYC" button
    When I navigate to "LE360overview" screen
    When I click on "LEDetails" from LHS 
    And I click on "IndustryCodeDetails" button to take screenshot
    Given I login to Fenergo Application with "KYCMaker: Corporate" 
		When I search for the "CaseId"
		Then I store the "CaseId" from LE360 
    When I navigate to "EnrichKYCProfileGrid" task
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task 
		When I Initiate "Fircosoft" by rightclicking 
		And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
		Then I complete "CompleteAML" task
    
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    
    When I navigate to "CompleteRiskAssessmentGrid" task 
		When I complete "RiskAssessment" task 
    
    Then I login to Fenergo Application with "RM:IBG-DNE"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"    
    When I refer back the case to "Enrich Client Information" stage
   	
   	Given I login to Fenergo Application with "KYCMaker: Corporate" 
		When I search for the "CaseId"
		Then I store the "CaseId" from LE360 
		When I navigate to "EnrichKYCProfileGrid" task  
    And I click on "SaveandCompleteforEnrichKYC" button
    
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I do "DocumentUpload" for all pending documents
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task 
		When I Initiate "Fircosoft" by rightclicking 
		And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
		Then I complete "CompleteAML" task 
    
    When I navigate to "CompleteID&VGrid" task 
		When I complete "CompleteID&V" task 
    
    When I navigate to "CompleteRiskAssessmentGrid" task 
		When I complete "RiskAssessment" task 
    
    Then I login to Fenergo Application with "RM:IBG-DNE" 
		When I search for the "CaseId" 
		When I navigate to "ReviewSignOffGrid" task 
		When I complete "ReviewSignOff" task 
		
		Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
		When I search for the "CaseId" 
		When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
		When I complete "ReviewSignOff" task 
		
		Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
		When I search for the "CaseId" 
		When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
		When I complete "ReviewSignOff" task 
		
		Then I login to Fenergo Application with "BUH:IBG-DNE" 
		When I search for the "CaseId" 
		When I navigate to "BHUReviewandSignOffGrid" task 
		When I complete "ReviewSignOff" task 
		
		Then I login to Fenergo Application with "KYCMaker: Corporate" 
		When I search for the "CaseId" 
		And I complete "Waiting for UID from GLCMS" task from Actions button
		When I navigate to "CaptureFabReferencesGrid" task 
		When I complete "CaptureFABReferences" task 
		And I assert that the CaseStatus is "Closed"
		
		When I navigate to "LE360overview" screen
    When I click on "LEDetails" from LHS 
    And I click on "IndustryCodeDetails" button to take screenshot
    #Verify the values are retained for three fields in Verified LE details screen of LE360 screen
    
    
    
    #================Below steps are written for manual testing
    #Given I login to Fenergo Application with "RM:IBG-DNE"
    #When I complete "NewRequest" screen with key "Corporate"
    #And I complete "CaptureNewRequest" with Key "C1" and below data
      #| Product | Relationship |
      #| C1      | C1           |
    #And I click on "Continue" button
    #When I complete "ReviewRequest" task
    #Then I store the "CaseId" from LE360
    #Given I login to Fenergo Application with "KYCMaker: Corporate"
    #When I search for the "CaseId"
    #When I navigate to "ValidateKYCandRegulatoryGrid" task
    #When I complete "ValidateKYC" screen with key "C1"
    #And I click on "SaveandCompleteforValidateKYC" button
    #When I navigate to "EnrichKYCProfileGrid" task
    #When I add a "AnticipatedTransactionActivity" from "EnrichKYC"
    #When I complete "AddAddressFAB" task
    #Then I store the "CaseId" from LE360
    #When I navigate to "EnrichKYC" task
    #Verify the Existing lov '00005-Individual and Trusts' is renamed as '00005-Trusts
    #Existing Lovs:547, New Lovs:79. Total Lovs: 626
    #Validate the LOV against PBI for the field 'Primary Industry of Operation'
    #Validate the LOV against PBI for the field 'Secondary Industry of Operation'
    #Validate the LOV against PBI for the field 'Primary Industry of Operation-UAE'
    #And I click on "SaveforLater" button
    #Verify the entered values for all three fields are updated correctly in LE360 LEdetails screen
    #When I complete "EnrichKYC" screen with key "C1"
    #And I click on "SaveandCompleteforEnrichKYC" button
    #When I navigate to "CaptureHierarchyDetailsGrid" task
    #When I add AssociatedParty by right clicking
    #When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    #When I complete "AssociationDetails" screen with key "Director"
    #When I complete "CaptureHierarchyDetails" task
    #When I navigate to "KYCDocumentRequirementsGrid" task
    #Then I store the "CaseId" from LE360
    #When I add a "DocumentUpload" in KYCDocument
    #Then I complete "KYCDocumentRequirements" task
    #When I navigate to "CompleteAMLGrid" task
    #When I Initiate "Fircosoft" by rightclicking
    #And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    #And I click on "SaveandCompleteforAssessmentScreen1" button
    #Then I complete "CompleteAML" task
    #When I navigate to "CompleteID&VGrid" task
    #When I complete "ID&V" task
    #When I complete "EditID&V" task
    #When I complete "AddressAddition" in "Edit Verification" screen
    #When I complete "Documents" in "Edit Verification" screen
    #When I complete "TaxIdentifier" in "Edit Verification" screen
    #When I complete "LE Details" in "Edit Verification" screen
    #When I click on "SaveandCompleteforEditVerification" button
    #When I complete "CompleteID&V" task
    #When I navigate to "CaptureRiskCategoryGrid" task
    #When I complete "RiskAssessmentFAB" task
    #Then I login to Fenergo Application with "RM:IBG-DNE"
    #When I search for the "CaseId"
    #When I navigate to "ReviewSignOffGrid" task
    #When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    #When I search for the "CaseId"
    #Refer back to Enrich Client Information stage and check the values are retained for the three fields
    #When I navigate to "EnrichKYC" task
    #Verify the values are retained for the three fields
    #When I complete "EnrichKYC" screen with key "C1"
    #And I click on "SaveandCompleteforEnrichKYC" button
    #When I navigate to "CaptureHierarchyDetailsGrid" task
    #When I add AssociatedParty by right clicking
    #When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    #When I complete "AssociationDetails" screen with key "Director"
    #When I complete "CaptureHierarchyDetails" task
    #When I navigate to "KYCDocumentRequirementsGrid" task
    #Then I store the "CaseId" from LE360
    #When I add a "DocumentUpload" in KYCDocument
    #Then I complete "KYCDocumentRequirements" task
    #When I navigate to "CompleteAMLGrid" task
    #When I Initiate "Fircosoft" by rightclicking
    #And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    #And I click on "SaveandCompleteforAssessmentScreen1" button
    #Then I complete "CompleteAML" task
    #When I navigate to "CompleteID&VGrid" task
    #When I complete "ID&V" task
    #When I complete "EditID&V" task
    #When I complete "AddressAddition" in "Edit Verification" screen
    #When I complete "Documents" in "Edit Verification" screen
    #When I complete "TaxIdentifier" in "Edit Verification" screen
    #When I complete "LE Details" in "Edit Verification" screen
    #When I click on "SaveandCompleteforEditVerification" button
    #When I complete "CompleteID&V" task
    #When I navigate to "CaptureRiskCategoryGrid" task
    #When I complete "RiskAssessmentFAB" task
    #Then I login to Fenergo Application with "RM:IBG-DNE"
    #When I search for the "CaseId"
    #When I navigate to "ReviewSignOffGrid" task
    #When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    #When I search for the "CaseId"
    #When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    #When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    #When I search for the "CaseId"
    #When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    #When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "BUH:IBG-DNE"
    #When I search for the "CaseId"
    #When I navigate to "BHUReviewandSignOffGrid" task
    #When I complete "ReviewSignOff" task
    #Then I login to Fenergo Application with "KYCMaker: Corporate"
    #When I search for the "CaseId"
    #When I navigate to "CaptureFabReferencesGrid" task
    #When I complete "CaptureFABReferences" task
    #And I assert that the CaseStatus is "Closed"
    #Verify the values are retained for three fields in Verified LE details screen of LE360 screen
