#Test Case: TC_WAC_Corporate_16
#PBI: WAC Corporate RiskAssessmentModel
#User Story ID: N/A
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_WAC_Corporate_16

  Scenario: COB-Refer back and derive the overrall risk rating as Low (previous risk rating Very High)
    #Refer to TC16 in the WAC Corp Data Sheet