#Test Case: TC_R2EPIC014PBI001_10
#PBI: R2EPIC014PBI001
#User Story ID: Corp/PCG-3, FIG-3
#Designed by: Priyanka Arora
#Last Edited by: Priyanka Arora
Feature: TC_R2EPIC014PBI001_10

  Scenario: 
    ##Validate "regular Review" workflow gets triggered in the system automatically when "Review Start date is same as current date" for Client type 'FI' for DM workflow
    ##Validate "regular Review" workflow gets triggered in the system automatically when "Review Start date is same as current date" for Client type 'NBFI' for DM workflow
    ##Validate "regular Review" workflow gets triggered in the system automatically when "Review Start date is same as current date" for Client type 'Corporate' for DM workflow
    ##Validate "regular Review" workflow gets triggered in the system bautomatically when "Review Start date is same as current date" for Client type 'BBG' for DM workflow
    ##Validate "regular Review" workflow gets triggered in the system automatically when "Review Start date is same as current date" for for Client type 'PCG Entity' for DM workflow
    #Pre-requisite: DM LE with 'T24 CIF ID' and Client type 'FI' with case status as 'closed'
    Given I login to Fenergo Application with "SuperUser"
    When I click on plus button and select 'New DM request' option
    When I selet 'Existing'option from 'DMrequestType' drop-down
    And I provide value for "T24 CIF ID" field
    Then I navigated to LE details screen
    And I assert that the CaseStatus is "Closed"
    #Test-data: "regular Review" workflow is triggered automatically when "Review Start date is same as current date" for this case
    When I navigate to "LE360-LE details" screen
    When I validate "Review Start date is same as current date"
    When I navigate to 'Cases' tab
    Then I see "regular Review" workflow is triggered for this case
    And I see under Tasks grid 'Close Associated Cases' task is triggered with 'Open' status
    ##Validate "regular Review" workflow gets triggered in the system automatically when "Review Start date is same as current date" for Client type 'NBFI' for DM workflow
    #Pre-requisite: DM LE with 'T24 CIF ID' and Client type 'NBFI' with case status as 'closed'
    Given I login to Fenergo Application with "SuperUser"
    When I click on plus button and select 'New DM request' option
    When I selet 'Existing'option from 'DMrequestType' drop-down
    And I provide value for "T24 CIF ID" field
    Then I navigated to LE details screen
    And I assert that the CaseStatus is "Closed"
    #Test-data: "regular Review" workflow is triggered automatically when "Review Start date is same as current date" for this case
    When I navigate to "LE360-LE details" screen
    When I validate "Review Start date is same as current date"
    When I navigate to 'Cases' tab
    Then I see "regular Review" workflow is triggered for this case
    And I see under Tasks grid 'Close Associated Cases' task is triggered with 'Open' status
    ##Validate "regular Review" workflow gets triggered in the system automatically when "Review Start date is same as current date" for Client type 'Corporate' for DM workflow
    #Pre-requisite: DM LE with 'T24 CIF ID' and Client type 'Corporate' with case status as 'closed'
    Given I login to Fenergo Application with "SuperUser"
    When I click on plus button and select 'New DM request' option
    When I selet 'Existing'option from 'DMrequestType' drop-down
    And I provide value for "T24 CIF ID" field
    Then I navigated to LE details screen
    And I assert that the CaseStatus is "Closed"
    #Test-data: "regular Review" workflow is triggered automatically when "Review Start date is same as current date" for this case
    When I navigate to "LE360-LE details" screen
    When I validate "Review Start date is same as current date"
    When I navigate to 'Cases' tab
    Then I see "regular Review" workflow is triggered for this case
    And I see under Tasks grid 'Close Associated Cases' task is triggered with 'Open' status
    ##Validate "regular Review" workflow gets triggered in the system bautomatically when "Review Start date is same as current date" for Client type 'BBG' for DM workflow
    #Pre-requisite: DM LE with 'T24 CIF ID' and Client type 'BBG' with case status as 'closed'
    Given I login to Fenergo Application with "SuperUser"
    When I click on plus button and select 'New DM request' option
    When I selet 'Existing'option from 'DMrequestType' drop-down
    And I provide value for "T24 CIF ID" field
    Then I navigated to LE details screen
    And I assert that the CaseStatus is "Closed"
    #Test-data: "regular Review" workflow is triggered automatically when "Review Start date is same as current date" for this case
    When I navigate to "LE360-LE details" screen
    When I validate "Review Start date is same as current date"
    When I navigate to 'Cases' tab
    Then I see "regular Review" workflow is triggered for this case
    And I see under Tasks grid 'Close Associated Cases' task is triggered with 'Open' status
    ##Validate "regular Review" workflow gets triggered in the system automatically when "Review Start date is same as current date" for for Client type 'PCG Entity' for DM workflow
    #Pre-requisite: DM LE with 'T24 CIF ID' and Client type 'PCG Entity' with case status as 'closed'
    Given I login to Fenergo Application with "SuperUser"
    When I click on plus button and select 'New DM request' option
    When I selet 'Existing'option from 'DMrequestType' drop-down
    And I provide value for "T24 CIF ID" field
    Then I navigated to LE details screen
    And I assert that the CaseStatus is "Closed"
    #Test-data: "regular Review" workflow is triggered automatically when "Review Start date is same as current date" for this case
    When I navigate to "LE360-LE details" screen
    When I validate "Review Start date is same as current date"
    When I navigate to 'Cases' tab
    Then I see "regular Review" workflow is triggered for this case
    And I see under Tasks grid 'Close Associated Cases' task is triggered with 'Open' status
