#BBGTest Case: TC_R2S1EPIC012PBI001&2&3_11
#PBI: R2S1EPIC012PBI001, R2S1EPIC012PBI002, R2S1EPIC012PBI003
#User Story ID: Light_KYC_001, Light_KYC_006, Light_KYC_015
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: TC_R2S1EPIC012PBI001&2&3_11-Lite KYC

  @Automation
  Scenario: Validate if Lite KYC is not triggered when "Does the CDD profile qualify for Lite KYC?" field is selected as "No" for "FI" client type and LE Category "Financial Institutions-Banks (Non-Vostro Relationship)/NBFI Non-CASA Relationship"
    ##Validate if risk is not defaulted to "Medium" and editable
    
    Given I login to Fenergo Application with "RM:FI"
    When I navigate to "LegalEntityCategoryWithoutSubmit" button with ClientType as "Financial Institution (FI)"
    When I select "Financial Institutions-Banks (Non-Vostro Relationship)/NBFI Non-CASA Relationship" for "Dropdown" field "Legal Entity Category"
    And I check that below data is visible
      | FieldLabel                                 |
      | Does the CDD profile qualify for Lite KYC? |
    And I validate the following fields in "Complete" Screen
      | Label                                      | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
      | Does the CDD profile qualify for Lite KYC? | Dropdown  | true    | false    | true      | Select...  |
    
    When I select "Client/Counterparty" for "Dropdown" field "Legal Entity Role"
    When I select "Bank - France Branch" for "Dropdown" field "Entity of Onboarding"
    When I select "Financial Institutions-Banks (Non-Vostro Relationship)/NBFI Non-CASA Relationship" for "Dropdown" field "Legal Entity Category"
    When I select "No" for "Dropdown" field "Does the CDD profile qualify for Lite KYC?"
    And I click on "CreateEntity" button
    When I navigate to "LE360overview" screen
    When I navigate to "Cases" from LHN section
    Then I Validate the CaseName doesnot contain "Lite KYC Onboarding" in it
    And I navigate to "CaptureRequestDetailsGrid" task
    And I complete "CaptureNewRequest" with Key "FI" and below data
      | Product | Relationship |
      | C1      | C1           |
    And I click on "Continue" button
    When I complete "ReviewRequest" task
    Then I store the "CaseId" from LE360
    
    Given I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "FI"
    And I click on "SaveandCompleteforValidateKYC" button
    When I navigate to "EnrichKYCProfileGrid" task
    When I complete "AddAddressFAB" task
    
    When I complete "EnrichKYC" screen with key "FI"
    And I click on "SaveandCompleteforEnrichKYC" button
    When I navigate to "CaptureHierarchyDetailsGrid" task
    When I add AssociatedParty by right clicking
    When I complete "AssociatedPartiesExpressAddition" screen with key "Non-Individual"
    When I complete "AssociationDetails" screen with key "Director"
    When I complete "CaptureHierarchyDetails" task
    
    When I navigate to "KYCDocumentRequirementsGrid" task
    When I add a "DocumentUpload" in KYCDocument
    Then I complete "KYCDocumentRequirements" task
    
    When I navigate to "CompleteAMLGrid" task
    When I Initiate "Fircosoft" by rightclicking
    And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    Then I complete "CompleteAML" task
    When I navigate to "CompleteID&VGrid" task
    When I complete "CompleteID&V" task
    
    When I navigate to "CompleteRiskAssessmentGrid" task 
		When I complete "RiskAssessment" task
    
    Then I login to Fenergo Application with "RM:FI"
    When I search for the "CaseId"
    When I navigate to "ReviewSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP"
    When I search for the "CaseId"
    When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "BUH:FI"
    When I search for the "CaseId"
    When I navigate to "BHUReviewandSignOffGrid" task
    When I complete "ReviewSignOff" task
    Then I login to Fenergo Application with "KYCMaker: FIG"
    When I search for the "CaseId"
    And I complete "Waiting for UID from GLCMS" task from Actions button 
     When I navigate to "CaptureFabReferencesGrid" task 
     When I complete "CaptureFABReferences" task 
    And I assert that the CaseStatus is "Closed"
    
    
    
    
    
    #Given I login to Fenergo Application with "RM:FI"
    #When I click on "+" sign to create new request
    #When I navigate to "Enter Entity details" screen
    #Test data: Client Type - FI
    #When I complete "Enter Entity details" screen task with ClientEntityType as "FI"
    #When I complete "Search For Duplicates" screen task
    #Then I select "Financial Institutions-Banks (Non-Vostro Relationship)/NBFI Non-CASA Relationship" for Legal Entity Category field
    #And I select "Client/Counterparty" for "Legal Entity Role" field
    #And I select any value for "Entity of Onboarding" field
    #And I assert "CREATE ENTITY" button is not enabled
    #And I select "No" for "Does the CDD profile qualify for Lite KYC?" field
    #And I assert "CREATE ENTITY" button is enabled
    #And I click on "CREATE ENTITY" button
    ##Validate Lite KYC is not triggered
    #And I assert "Lite KYC Onboarding" workflow is not triggered
    #Validate if the user is directly taken to Capture Request Details screen of COB workflow
    #And I assert user is navigated to "Capture Request Details" screen #COB workflow
    #Test data - Confidential value - FI
    #And I complete "CaptureNewRequest" with Key "C1" and below data
      #| Product | Relationship |
      #| C1      | C1           |
    #And I click on "Continue" button
    #When I complete "ReviewRequest" task
    #Then I store the "CaseId" from LE360
    #Given I login to Fenergo Application with "KYCMaker: FIG"
    #When I search for the "CaseId"
    #When I navigate to "ValidateKYCandRegulatoryGrid" task
    #When I complete "ValidateKYC" screen with key "C1"
    #And I click on "SaveandCompleteforValidateKYC" button
    #When I complete "EnrichKYC" screen with key "C1"
    #And I click on "SaveandCompleteforEnrichKYC" button
    #Add an associated party
    #When I navigate to "CaptureHierarchyDetailsGrid" task
    #Then I add an associated party
    #When I complete "CaptureHierarchyDetails" task
    #When I navigate to "KYCDocumentRequirementsGrid" task
    ##Validate Doc Matrix requirement for COB work flow with Client Type "FI"  and LE Category "Financial Institutions-Banks (Non-Vostro Relationship)/NBFI Non-CASA Relationship" is trigerred
    #Then I click on "Save & Complete
    #Then I assert Error is thrown to add mandatory documents
    #When I add a "DocumentUpload" in KYCDocument # add only mandaotry documents
    #Then I complete "KYCDocumentRequirements" task
    #When I navigate to "CompleteAMLGrid" task
    #When I Initiate "Fircosoft" by rightclicking
    #And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData"
    #And I click on "SaveandCompleteforAssessmentScreen1" button
    #Then I complete "CompleteAML" task
    #When I navigate to "CompleteID&VGrid" task
    #When I complete "CompleteID&V" task
    #When I navigate to "CaptureRiskCategoryGrid" task
    ##Validate if risk is not defaulted to "Medium" and is editable
    #And I assert "Risk Category" is blank
    #And I assert "Risk Category" is editable
    #And I assert "Continue" button is disabled
    #And I select "Medium" for "Risk Category" field
    #When I complete "RiskAssessmentFAB" task
    #Then I login to Fenergo Application with "RM"
    #When I search for the "CaseId"
    #When I complete "ReviewSignOff" task #Relationship Manager Review and Sign-Off
    #Then I login to Fenergo Application with "KYCManager"
    #When I search for the "CaseId"
    #When I complete "ReviewSignOff" task #CIB R&C KYC Approver - KYC Manager Review and Sign-Off
    #Then I login to Fenergo Application with "AVP"
    #When I search for the "CaseId"
    #When I complete "ReviewSignOff" task #CIB R&C KYC Approver - AVP Review and Sign-Off
    #Then I login to Fenergo Application with "BUH:FI"
    #When I search for the "CaseId"
    #When I complete "ReviewSignOff" task #Business Unit Head Review and Sign-Off
    