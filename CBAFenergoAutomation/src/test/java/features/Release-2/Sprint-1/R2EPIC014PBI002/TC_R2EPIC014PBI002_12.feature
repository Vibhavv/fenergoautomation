#Test Case: TC_R2EPIC014PBI002_12
#PBI: R2EPIC014PBI002
#User Story ID: Corp/PCG-7, FIG-7
#Designed by: Anusha PS
#Last Edited by: Anusha PS
Feature: TC_R2EPIC014PBI002_12

  Scenario: Validate the subflows of new task "Review Request" task
    #Tax Identifier
    #Trading Entities
    #Products
    #Relationships
    #Addresses
    #Contacts
    #Documents
    #Precondition: Closed COB case with Client type as Corporte with Country of Incorpoation as "UAE" and data for non mandatory subflows Tax Identifier, Trading Entities and Contacts
    
     Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I complete "NewRequest" screen with key "Corporate" 
	When I complete "Contacts" task
	When I complete "TaxIdentifier" with TaxType as "VAT ID" and Country as "AE-UNITED ARAB EMIRATES" for "CaptureRequestDetails"
	And I complete "CaptureNewRequest" with Key "C1" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
	When I complete "AddAddressFAB" task 
	When I complete "EnrichKYC" screen with key "C1" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I add AssociatedParty by right clicking 
	When I add "AssociatedParty" via express addition 
	When I complete "AssociationDetails" screen with key "Director" 
	When I complete "CaptureHierarchyDetails" task 
	
	When I navigate to "KYCDocumentRequirementsGrid" task 
	Then I compare the list of documents should be same as "COBDocument" for ClientType "Corporate" 
	
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	
	When I navigate to "CompleteAMLGrid" task 
	When I Initiate "Fircosoft" by rightclicking 
	And I complete "Fircosoft" from assessment grid with Key "FicrosoftScreeningData" 
	And I click on "SaveandCompleteforAssessmentScreen1" button 
	Then I complete "CompleteAML" task 
	
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	
	When I navigate to "CaptureRiskCategoryGrid" task 
	When I complete "RiskAssessment" screen with key "Low" 
	
	Then I login to Fenergo Application with "RM:IBG-DNE" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	
	Then I login to Fenergo Application with "BUH:IBG-DNE" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
#	
    Given I login to Fenergo Application with "KYCMaker: Corporate"
    When I search for the "CaseId"
    #Initiate Regular Review
    When I select "RegularReview" from Actions menu
    And I store the "CaseId" from LE360
    When I navigate to "CloseAssociatedCases" task
    And I click on "SaveandComplete" button
    When I navigate to "ValidateKYCandRegulatoryGrid" task
    When I complete "ValidateKYC" screen with key "C1"
    When I navigate to "ReviewRequestGrid" task
    
    
    
    
    
    
    
    
    
    
    
    
    
#    
#    And I validate the following subflows if the data is populated from COB
#      | Tax Identifier   |
#      | Trading Entities |
#      | Products         |
#      | Relationships    |
#      | Addresses        |
#      | Contacts         |
#      | Documents        |
#    And I validate if existing data (line item in the grid) can be modified
#    And I validate if existing data (line item in the grid) can be deleted
#    And I validate if new data  (line item in the grid) can be added for following subflows
