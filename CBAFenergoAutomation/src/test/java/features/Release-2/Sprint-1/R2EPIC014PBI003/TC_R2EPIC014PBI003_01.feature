#Test Case: TC_R2EPIC014PBI003_01
#PBI: R2EPIC014PBI003
#User Story ID:
#Designed by: Niyaz Ahmed
#Last Edited by: Niyaz Ahmed
Feature: TC_R2EPIC014PBI003_01 

@Automation 
Scenario:
Corporate:Validate the field behaviours in Customer Details section of Review/Edit Client Data Screen in RR workflow 
#Precondition: COB case with Client type as Corporte and COI as UAE to be created by filling all the mandatory and non-mandatory fields and Case status should be Closed
	Given I login to Fenergo Application with "RM:IBG-DNE" 
	When I complete "NewRequest" screen with key "Corporate" 
	And I complete "CaptureNewRequest" with Key "C1" and below data 
		| Product | Relationship |
		| C1      | C1           |
	And I click on "Continue" button 
	When I complete "ReviewRequest" task 
	Then I store the "CaseId" from LE360 
	Given I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "EnrichKYCProfileGrid" task 
	When I add a "AnticipatedTransactionActivity" from "EnrichKYC" 
	When I complete "AddAddressFAB" task 
	Then I store the "CaseId" from LE360 
	When I complete "EnrichKYC" screen with key "C1" 
	And I click on "SaveandCompleteforEnrichKYC" button 
	When I navigate to "CaptureHierarchyDetailsGrid" task 
	When I complete "CaptureHierarchyDetails" task 
	When I navigate to "KYCDocumentRequirementsGrid" task 
	Then I store the "CaseId" from LE360 
	When I add a "DocumentUpload" in KYCDocument 
	Then I complete "KYCDocumentRequirements" task 
	When I navigate to "CompleteAMLGrid" task 
	Then I complete "CompleteAML" task 
	When I navigate to "CompleteID&VGrid" task 
	When I complete "CompleteID&V" task 
	When I navigate to "CompleteRiskAssessmentGrid" task 
	When I complete "RiskAssessment" task 
	Then I login to Fenergo Application with "RM:IBG-DNE" 
	When I search for the "CaseId" 
	When I navigate to "ReviewSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - KYC Manager" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApprover-KYCManagerReviewandSign-Off" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "CIB R&C KYC APPROVER - AVP" 
	When I search for the "CaseId" 
	When I navigate to "CIBR&CKYCApproverAVPReviewGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "BUH:IBG-DNE" 
	When I search for the "CaseId" 
	When I navigate to "BHUReviewandSignOffGrid" task 
	When I complete "ReviewSignOff" task 
	Then I login to Fenergo Application with "KYCMaker: Corporate" 
	When I search for the "CaseId" 
	Then I store the "CaseId" from LE360 
	And I complete "Waiting for UID from GLCMS" task from Actions button 
	When I navigate to "CaptureFabReferencesGrid" task 
	When I complete "CaptureFABReferences" task 
	And I assert that the CaseStatus is "Closed" 
	#     =Initiate Regular Review
	And I initiate "Regular Review" from action button 
	#	=Then I navigate to "CloseAssociatedCasesGrid" task
	When I complete "CloseAssociatedCase" task 
	Then I store the "CaseId" from LE360 
	When I navigate to "ValidateKYCandRegulatoryGrid" task 
	When I complete "ValidateKYC" screen with key "C1" 
	And I click on "SaveandCompleteforValidateKYC" button 
	When I navigate to "ReviewRequestGrid" task 
	And I validate the following fields in "Customer Details" Sub Flow 
		| Label               | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
		| Channel & Interface | Dropdown  | true    | false    | true      | NA         |
	And I verify "Channel & Interface" drop-down values 
	When I complete "RRReviewRequest" task 
	When I navigate to "Review/EditClientDataTask" task 
	And I validate the following fields in "Customer Details" Sub Flow 
		| Label                                   | FieldType | Visible | ReadOnly | Mandatory | DefaultsTo |
		| Client Type                             | Dropdown  | true    | true     | NA        | NA         |
		| Name of Registration Body               | TextBox   | true    | false    | true      | NA         |
		| Country of Domicile / Physical Presence | Dropdown  | true    | false    | true      | NA         |
		| Trading/Operation Name                  | TextBox   | true    | false    | false     | NA         |
		| Group Name                              | TextBox   | true    | false    | NA        | NA         |
		| FAB Segment                             | Dropdown  | true    | false    | true      | NA         |
	And I validate the following fields in "Customer Details" Sub Flow 
		| Label                                    | FieldType  | Visible | ReadOnly | Mandatory | DefaultsTo |
		| ID                                       | TextBox    | true    | true     | NA        | NA         |
		| Legal Entity Name                        | TextBox    | true    | false    | NA        | NA         |
		| Legal Entity Type                        | Dropdown   | true    | true     | NA        | NA         |
		| Entity Type                              | Dropdown   | true    | true     | NA        | NA         |
		| Legal Entity Category                    | Dropdown   | true    | true     | NA        | NA         |
		| LEI                                      | TextBox    | true    | false    | false     | NA         |
		| GLEIF Legal Name                         | NA         | false   | NA       | NA        | NA         |
		| GLEIF Legal Jurisdiction                 | NA         | false   | NA       | NA        | NA         |
		| GLEIF Legal Form                         | NA         | false   | NA       | NA        | NA         |
		| GLEIF Entity Status                      | NA         | false   | NA       | NA        | NA         |
		| Country of Incorporation / Establishment | Dropdown   | true    | false    | true      | NA         |
		| Date of Incorporation / Establishment    | DatePicker | true    | false    | true      | NA         |
		| Registration Number                      | TextBox    | true    | false    | true      | NA         |
		| Customer Relationship Status             | Dropdown   | true    | false    | NA        | NA         |
		| Length of Relationship                   | Dropdown   | true    | false    | true      | NA         |
		| Does the entity have a previous name(s)? | Dropdown   | true    | false    | true      | NA         |
		| Previous Name(s)                         | TextBox    | false    | false    | true      | NA         |
		| Legal Entity Name (Parent)               | TextBox    | true    | false    | NA        | NA         |
		| Website Address                          | TextBox    | true    | false    | NA        | NA         |
		| Real Name                                | TextBox    | true    | false    | NA        | NA         |
		| Original Name                            | TextBox    | true    | false    | NA        | NA         |
		| Legal Counterparty Type                  | Dropdown   | true    | false    | true      | NA         |
		| Legal Constitution Type                  | Dropdown   | true    | false    | true      | NA         |
		| Emirate                                  | Dropdown   | true    | false    | true      | NA         |
		| Entity Level                             | NA         | false   | NA       | NA        | NA         |
		| SWIFT Address                            | NA         | false   | NA       | NA        | NA         |
	And I check that below data is not visible 
		| FieldLabel                      |
		| Legal Status                    |
		| Anticipated Activity of Account |
		| Primary Business Location       |
	And I fill "Alphanumeric" data of length "256" in "Name of Registration Body" field 
	Then I validate the error messgage for "Name of Registration Body" as "You've reached the maximum length. Name of Registration Body accepts 255 characters." 
	And I fill "Alphanumeric" data of length "254" in "Name of Registration Body" field 
	And I fill "Alphanumeric" data of length "255" in "Name of Registration Body" field 
	And I fill "SpecialCharacters" data of length "20" in "Trading/Operation Name" field 
	And I take a screenshot 
	And I fill "LowerCaseCharacters" data of length "30" in "Trading/Operation Name" field 
	And I take a screenshot 
	And I fill "Numeric" data of length "35" in "Trading/Operation Name" field 
	And I take a screenshot 
	And I fill "UpperCaseCharacters" data of length "36" in "Trading/Operation Name" field 
	And I take a screenshot 
	And I fill "UpperCaseCharacters" data of length "35" in "Trading/Operation Name" field 
	And I take a screenshot 
	And I fill "UpperCaseCharacters" data of length "34" in "Trading/Operation Name" field 
	And I take a screenshot 
	And I fill "Alphanumeric" data of length "256" in "GroupName" field 
	Then I validate the error messgage for "GroupName" as "You've reached the maximum length. Group Name accepts 255 characters." 
	And I fill "Alphanumeric" data of length "254" in "GroupName" field 
	And I fill "Alphanumeric" data of length "255" in "GroupName" field 
	And I fill "SpecialCharacters" data of length "200" in "LegalEntityName(Parent)" field 
	And I take a screenshot 
	And I fill "LowerCaseCharacters" data of length "255" in "LegalEntityName(Parent)" field 
	And I take a screenshot 
	And I fill "Numeric" data of length "255" in "LegalEntityName(Parent)" field 
	And I take a screenshot 
	And I fill "UpperCaseCharacters" data of length "260" in "LegalEntityName(Parent)" field 
	And I take a screenshot 
	And I fill "UpperCaseCharacters" data of length "255" in "LegalEntityName(Parent)" field 
	And I take a screenshot 
	And I fill "UpperCaseCharacters" data of length "254" in "LegalEntityName(Parent)" field 
	And I take a screenshot 
	And I fill "Alphanumeric" data of length "256" in "WebsiteAddress" field 
	And I take a screenshot 
	And I fill "Alphanumeric" data of length "254" in "WebsiteAddress" field 
	And I take a screenshot 
	And I fill "Alphanumeric" data of length "255" in "WebsiteAddress" field 
	And I take a screenshot 
	And I fill "Alphanumeric" data of length "256" in "RealName" field 
	Then I validate the error messgage for "RealName" as "You've reached the maximum length. Real Name accepts 255 characters." 
	And I fill "Alphanumeric" data of length "254" in "RealName" field 
	And I take a screenshot 
	And I fill "Alphanumeric" data of length "255" in "RealName" field 
	And I take a screenshot 
	And I fill "Alphanumeric" data of length "256" in "OriginalName" field 
	Then I validate the error messgage for "OriginalName" as "You've reached the maximum length. Original Name accepts 255 characters." 
	And I fill "Alphanumeric" data of length "254" in "OriginalName" field 
	And I take a screenshot 
	And I fill "Alphanumeric" data of length "255" in "OriginalName" field 
	And I take a screenshot 
	And I verify "Country of Domicile / Physical Presence" drop-down values 
	And I verify "Country of Incorporation / Establishment" drop-down values 
	And I verify "Length of Relationship" drop-down values 
	And I verify "Legal Counterparty Type" drop-down values 
	And I verify "Legal Constitution Type" drop-down values 
	And I verify "FAB Segment" drop-down values 
	
	
	
	
	#=Below steps are written for manual testing
	#Given I login to Fenergo Application with "KYCMaker: Corporate"
	#When I search for the "CaseId"
	#Initiate Regular Review
	#When I select "RegularReview" from Actions menu
	#When I navigate to "CloseAssociatedCases" task
	#And I click on "SaveandComplete" button
	#When I navigate to "ValidateKYCandRegulatoryGrid" task
	#When I complete "ValidateKYC" screen with key "C1"
	#And I click on "SaveandCompleteforValidateKYC" button
	#When I navigate to "ReviewEditClientData" task
	#Validate in customer details section the below fields are available (Label change) and values are autopopulated from the COB case
	#Validate the field type, visibility, editable and mandatory and field values are defautled from COB
	#And I validate the following fields in "Customer Details" Sub Flow
	#| FieldLabel                              | Field Type   | Visible | Editable | Mandatory | Field Defaults To       |
	#| Client Type                             | Drop-down    | True    | False    | False     | Auto populated from COB |
	#| Name of Registration Body               | Alphanumeric | True    | True     | True      | Auto populated from COB |
	#| Country of Domicile / Physical Presence | Drop-down    | True    | True     | True      | Auto populated from COB |
	#| Channel & Interface                     | Drop-down    | True    | True     | True      | Auto populated from COB |
	#| Trading/Operation Name                  | Alphanumeric | True    | True     | False     | Auto populated from COB |
	#| Group Name                              | Alphanumeric | True    | True     | False     | Auto populated from COB |
	#| FAB Segment                             | Drop-down    | True    | True     | True      | Auto populated from COB |
	#Validate the below new fields & Existing fields are available in customer details section and values are autopopulated from COB case
	#Validate the field type, visibility, editable and mandatory and field values are defautled from COB
	#Entity level and Swift Address fields will be visible when Client type is FI/NBFI
	#Emirate field will be visible when COI = UAE
	#And I validate the following fields in "Customer Details" Sub Flow
	#| FieldLabel                               | Field Type   | Visible | Editable | Mandatory | Field Defaults To       |
	#| ID                                       | Alphanumeric | True    | False    | False     | Auto populated from COB |
	#| Legal Entity Name                        | Alphanumeric | True    | False    | False     | Auto populated from COB |
	#| Legal Entity Type                        | Drop-down    | True    | False    | False     | Auto populated from COB |
	#| Entity Type                              | Drop-down    | True    | False    | False     | Auto populated from COB |
	#| Legal Entity Category                    | Drop-down    | True    | False    | False     | Auto populated from COB |
	#| LEI                                      | Alphanumeric | True    | True     | False     | Auto populated from COB |
	#| GLEIF Legal Name                         | Alphanumeric | True    | True     | False     | Auto populated from COB |
	#| GLEIF Legal Jurisdiction                 | Alphanumeric | True    | True     | False     | Auto populated from COB |
	#| GLEIF Legal Form                         | Alphanumeric | True    | True     | False     | Auto populated from COB |
	#| GLEIF Entity Status                      | Alphanumeric | True    | True     | False     | Auto populated from COB |
	#| Country of Incorporation / Establishment | Drop-down    | True    | True     | True      | Auto populated from COB |
	#| Date of Incorporation / Establishment    | Date         | True    | True     | True      | Auto populated from COB |
	#| Registration Number                      | Alphanumeric | True    | True     | True      | Auto populated from COB |
	#| Customer Relationship Status             | Drop-down    | True    | False    | False     | Auto populated from COB |
	#| Length of Relationship                   | Drop-down    | True    | True     | True      | Auto populated from COB |
	#| Does the entity have a previous name(s)? | Drop-down    | True    | True     | True      | Auto populated from COB |
	#| Previous Name(s)                         | Alphanumeric | True    | True     | True      | Auto populated from COB |
	#| Legal Entity Name (Parent)               | Alphanumeric | True    | True     | False     | Auto populated from COB |
	#| Website Address                          | Alphanumeric | True    | True     | False     | Auto populated from COB |
	#| Real Name                                | Alphanumeric | True    | True     | False     | Auto populated from COB |
	#| Original Name                            | Alphanumeric | True    | True     | False     | Auto populated from COB |
	#| Legal Counter party type                 | Drop-down    | True    | True     | True      | Auto populated from COB |
	#| Legal Constitution Type                  | Drop-down    | True    | True     | True      | Auto populated from COB |
	#| Emirate                                  | Drop-down    | True    | False    | True      | Auto populated from COB |
	#| Entity Level                             |              | False   |          |           |                         |
	#| SWIFT Address                            |              | False   |          |           |                         |
	#Validate the below fields are hidden in customer details section
	#And I validate the following fields in "Customer Details" Sub Flow
	#| Label                           | Visible |
	#| Legal Status                    | false   |
	#| Anticipated Activity of Account | false   |
	#| Primary Business Location       | false   |
	#| Residential Status              | false   |
	#Field Validation for 'Name of Registration Body' field
	#Verify 'Name of Registration Body' field NOT accepts more than 255 Alphanumeric characters. Test data : 256 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data8"
	#Verify Legal Entity Name field accepts less than 255 Alphanumeric characters. Test data : 254 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data9"
	#Verify Legal Entity Name field accepts 255 Alphanumeric characters. Test data : 255 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data10"
	##
	#Field validation for 'Trading/Operation Name' field
	#Verify 'Trading/Operation Name' field NOT accepts special characters. Test Data:Special characters
	#And I fill the data for "ReviewEditClientData" with key "Data1"
	#Verify 'Trading/Operation Name' field NOT accepts lower case characters. Test Data: Lower case char
	#And I fill the data for "ReviewEditClientData" with key "Data2"
	#Verify 'Trading/Operation Name' field accepts valid characters other than special and lowercase.
	#And I fill the data for "ReviewEditClientData" with key "Data3"
	#Verify 'Trading/Operation Name' field NOT accepts more than 150 Alphanumeric characters. Test data : 151 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data8"
	#Verify 'Trading/Operation Name' field accepts less than 150 Alphanumeric characters. Test data : 149 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data9"
	#Verify 'Trading/Operation Name' field accepts 150 Alphanumeric characters. Test data : 150 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data10"
	##
	#Field Validation for 'Group Name' field
	#Verify 'Group Name' field NOT accepts more than 255 Alphanumeric characters. Test data : 256 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data8"
	#Verify 'Group Name' field accepts less than 255 Alphanumeric characters. Test data : 254 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data9"
	#Verify 'Group Name' field accepts 255 Alphanumeric characters. Test data : 255 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data10"
	##
	#Field validation for 'Legal Entity Name (Parent)' field
	#Verify Legal Entity Name (parent) field NOT accepts special characters. Test Data:Special characters
	#And I fill the data for "ReviewEditClientData" with key "Data1"
	#Verify Legal Entity Name (parent) field NOT accepts lower case characters. Test Data: Lower case char
	#And I fill the data for "ReviewEditClientData" with key "Data2"
	#Verify Legal Entity Name (parent) field accepts valid characters other than special and lowercase.
	#And I fill the data for "ReviewEditClientData" with key "Data3"
	#Verify 'Legal Entity Name (parent)' field NOT accepts more than 255 Alphanumeric characters. Test data : 256 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data8"
	#Verify 'Legal Entity Name (parent)' field accepts less than 255 Alphanumeric characters. Test data : 254 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data9"
	#Verify 'Legal Entity Name (parent)' field accepts 255 Alphanumeric characters. Test data : 255 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data10"
	##
	#Field validation for 'Website Address' field
	#Verify Website field does not accept duplicate value. Test Data: Enter dupliate website address
	#Verify 'Website Address' field NOT accepts more than 255 Alphanumeric characters. Test data : 256 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data8"
	#Verify 'Website Address' field accepts less than 255 Alphanumeric characters. Test data : 254 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data9"
	#Verify 'Website Address' field accepts 255 Alphanumeric characters. Test data : 255 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data10"
	##
	#Field validation for 'Real Name' field
	#Verify 'Real Name' field NOT accepts more than 255 Alphanumeric characters. Test data : 256 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data8"
	#Verify 'Real Name' field accepts less than 255 Alphanumeric characters. Test data : 254 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data9"
	#Verify 'Real Name' field accepts 255 Alphanumeric characters. Test data : 255 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data10"
	##
	#Field validation for 'Original Name' field
	#Verify 'Original Name' field NOT accepts more than 255 Alphanumeric characters. Test data : 256 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data8"
	#Verify 'Original Name' field accepts less than 255 Alphanumeric characters. Test data : 254 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data9"
	#Verify 'Original Name' field accepts 255 Alphanumeric characters. Test data : 255 Alphanumeric characters
	#And I fill the data for "ReviewEditClientData" with key "Data10"
	##
	#Field validation for 'Date of Incorporation / Establishment' field
	#Verify 'Date of Incorporation / Establishment' field does not accepts future date.
	##
	#LOV validation - Country of Domicile/ Physical Presence. (Refer to Countries in the PBI-LOV tab)
	#And I validate the LOV of "CountryofDomicile/PhysicalPresence" with key "countrieslov"
	##
	#LOV validation - Channel & Interface. (Refer to Channellist in the PBI-LOV tab)
	#And I validate the LOV of "Channel&Interface" with key "Channellist"
	##
	#LOV validation - Country of Incorporation / Establishment. (Refer to Countries in the PBI-LOV tab)
	#And I validate the LOV of "CountryofIncorporation/Establishment" with key "countrieslov"
	##
	#LOV validation - Length of Relationship. (Refer to LengthofRelationship in the PBI-LOV tab)
	#And I validate the LOV of "LengthofRelationship" with key "LengthofRelationship"
	##
	#LOV validation - Legal Counter party type. (Refer to Legal Counterparty Type in the PBI-LOV tab)
	#And I validate the LOV of "LegalCounterpartytype" with key "LegalCounterpartyType"
	##
	#LOV validation - Legal Constitution Type. (Refer to Legal Constitution Type in the PBI-LOV tab)
	#And I validate the LOV of "LegalConstitutionType" with key "LegalConstitutionType"
	##
	#LOV validation - FAB Segment. (Refer FABsegmentcode&desc in the PBI-LOV tab)
	#And I validate the LOV of "FABSegment" with key "FABSegment"
